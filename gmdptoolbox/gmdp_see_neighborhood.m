function gmdp_see_neighborhood(GMDP, sites)

% gmdp_see_neighborhood     Visualize the neighborhood relations between sites
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% sites (optional) : vector of sites to be colored differently (default value: empty)
% Evaluation --------------------------------------------------------------
% none

% Check arguments
n=length(GMDP.G);
if nargin>1 && any(~ismember(sites, 1:n)) 
    disp('--------------------------------------------------------')
    disp('GMDP toolbox Error: sites must be a vector of sites in [1 n]')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
    
    %  Set optional arguments
    if nargin<2; sites=[]; end;
    
    % Plot graph using graphViz4Matlab toolbox
    % Sites are colored in [1 1 0.8] and circled with continuous line
    [~,h]=evalc('graphViz4Matlab(GMDP.G)');
    
    % Sites not neighbors with themselves are circled with dot line 
    for i = find(diag(GMDP.G)==0)
            set(h.nodeArray(i), 'lineStyle', ':', 'lineWidth', 0.01);
    end
    
    % specified sites are colored in a different color
    for i = sites 
        set(h.nodeArray(i),'faceColor',[0.85 0.85 0.7]);
    end
    h.redraw()
end

