function [policyloc, iter] = gmdp_policy_iteration_MF(GMDP,discount, max_iter, policyloc0, Qloc0, Tend_eval, max_iter_update, issimplified)


% gmdp_policy_iteration_MF  Resolution of discounted Grah-based MDP 
%                           with approximated policy iteration algorithm
%                           The evaluation of a policy is done with
%                           gmdp_eval_policy_MF
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% discount : discount rate in ]0; 1[
% max_iter (optional) : maximum number of iteration to be done (default value:100)
% policyloc0 (optional) : an initial policy (default value: ones)
% Qloc0 (optional) : initial approximated probability (default value: uniform distribution)
% Tend_eval (optional) : approximation of infinity (default value: 100)
% max_iter_update (optional) : maximum number of iteration to update current policy (default value: 10)
% issimplified (optional) : simplified MF approximation (default value: false)
% Evaluation --------------------------------------------------------------
% policyloc : approximated optimum policy
% iter : number of done iterations

threshold = 0.000001; % for numerical comparison

policyloc=[]; iter=[];
% check of arguments
n = length(GMDP.M);
isOKQloc0 = true; if (nargin > 4) && (length(Qloc0)~=n); isOKQloc0 = false; end
SN = zeros(n,1);
for i=1:n
    SN(i) = size(GMDP.VN{i},1);
    if (nargin > 4) && isOKQloc0 && ( length(Qloc0{i})~=GMDP.M(i) || ...
            any(Qloc0{i} <0) || any(Qloc0{i} >1) || abs(sum(Qloc0{i})-1)>threshold ); 
        isOKQloc0 = false; 
    end
end

if ~isnumeric(discount) || discount <= 0 || discount >= 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: discount argument must be in ]0,1[')
    disp('--------------------------------------------------------')
elseif (nargin > 2) && ( ~isnumeric(max_iter) || max_iter < 1 || floor(max_iter)~=max_iter ) 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: The maximum number of iteration must be an integer upper than 0')
    disp('--------------------------------------------------------')
elseif (nargin > 3) && ~gmdp_check_policy(GMDP, policyloc0, false)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: policyloc0 is not correct')
    disp('--------------------------------------------------------')
elseif ~isOKQloc0
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Qloc0 is not correct')
    disp('--------------------------------------------------------')
elseif (nargin > 5) && ( ~isnumeric(Tend_eval) || Tend_eval < 1 || floor(Tend_eval)~=Tend_eval ) 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Tend_eval must be an integer upper than 0')
    disp('--------------------------------------------------------')
elseif (nargin > 6) && ( ~isnumeric(max_iter_update) || max_iter_update < 1 || floor(max_iter_update)~=max_iter_update ) 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: The maximum number of update iteration must be an integer upper than 0')
    disp('--------------------------------------------------------')
elseif (nargin > 7) && ~islogical(issimplified) 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: argument issimplified must be a boolean')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')

    % initialization of optional arguments 
    if ~(nargin > 2); max_iter = 100; end
    if ~(nargin > 3); for i=1:n; [~,policyloc0{i}]=max(GMDP.Rloc{i},[],2);end ; end
    if ~(nargin > 4); for i=1:n; Qloc0{i} = (ones(GMDP.M(i),1)/GMDP.M(i)); end; end
    if ~(nargin > 5); Tend_eval = 100; end
    if ~(nargin > 6); max_iter_update = 10; end
    if ~(nargin > 7); issimplified = false; end
           
    policyloc = policyloc0; % preallocation
    policyloc_prev = policyloc0;
    iter = 0;
    isiter = true;
    while isiter
        Vloc = gmdp_eval_policy_MF(GMDP, discount, policyloc_prev, Qloc0, Tend_eval);
        v=0;for i=1:n;v=v+mean(Vloc{i});end; display(['Iteration: ' int2str(iter+1) '     Current approximate global value: ' num2str(v)]);
        policyloc_prev_update = policyloc_prev;
        iter_update = 0;
        isiterupdate = true;
        while isiterupdate
            
            % compute Ci(ai, sNi)
            for i = 1:n % for each site
                res=zeros(SN(i),1);
                parfor sN = 1:SN(i)
                    v_sN = GMDP.VN{i}(sN,:);
                    
                    C = GMDP.Rloc{i}(sN,:);
                    for a = 1:GMDP.B(i)
                       Z1 = 0;
                       for k = find(GMDP.G(i,:)) % sites with i as neightbor
                            N_k_without_i= GMDP.N{k}; 
                            N_k_without_i(GMDP.N{k}==i)=[]; % N(k) without i
                            Z2 = 0;
                            for ssN =1:SN(k)
                                v_ssN  = GMDP.VN{k}(ssN,:); 
                                Z3 = 1; 
                                for j = N_k_without_i
                                    % sites neightbor of k without i
                                    if issimplified
                                        if GMDP.G(j,i)==1
                                            N_j_and_i = j;
                                        else
                                            N_j_and_i = [];
                                        end
                                    else
                                        N_j_and_i = find(GMDP.G(:,j).*GMDP.G(:,i))';
                                    end
                                    
                                    Z4 = 0;
                                    x_v_sN_in_v_sssN = 0;
                                    for sssN = 1:SN(j)
                                        v_sssN = GMDP.VN{j}(sssN,:); 
                                        is_v_sN_in_v_sssN = true;
                                        if ~isempty(N_j_and_i)
                                            for d = N_j_and_i
                                                if v_sssN(GMDP.N{j}==d) ~= v_sN(GMDP.N{i}==d);
                                                    is_v_sN_in_v_sssN = false;
                                                end
                                            end
                                        end
                                        if is_v_sN_in_v_sssN
                                            x_v_sN_in_v_sssN = x_v_sN_in_v_sssN + 1;
                                            Z4 = Z4 + GMDP.Ploc{j}(sssN, v_ssN(GMDP.N{k}==j),policyloc_prev{j}(sssN));
                                        end
                                        
                                    end
                                    if  x_v_sN_in_v_sssN ~= 0
                                        Z3 = Z3 * Z4 / x_v_sN_in_v_sssN;
                                    end
                                end
                                Z3 = Z3 * Vloc{k}(ssN) * GMDP.Ploc{i}(sN, v_ssN(GMDP.N{k}==i),a);
                                Z2 = Z2 + Z3;
                            end
                            Z1 = Z1 + Z2;
                        end
                        C(a) = C(a) + discount*Z1;
                    end
                    
                    [~,res(sN)] = max(C);
                end
                policyloc{i} = res;
            end
            
            iter_update = iter_update + 1;
            isequal=true; for d=1:n; if any(policyloc_prev_update{d}~=policyloc{d}); isequal=false; end; end
            if isequal || iter_update == max_iter_update;
                isiterupdate = false;
            else
                policyloc_prev_update = policyloc;
            end
        end
        iter = iter + 1;
        isequal=true; for d=1:n; if any(policyloc_prev{d}~=policyloc{d}); isequal=false; end; end
        if isequal || iter == max_iter
            isiter = false;
        else
            policyloc_prev = policyloc;
        end
    end
end
