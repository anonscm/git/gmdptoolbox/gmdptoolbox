function policyloc_verbose = gmdp_see_policy_tab(GMDP, policyloc, sites, isdisplayed)

% gmdp_see_policy_tab   Display actions of a policy and detailed state of
% the neighborhood
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a given policy
% sites (optional) : vector of sites to be displayed (default value: all sites)
% isdisplayed (optional) : if true: display verbose policy, if false: no display
%                          (default value: true)
% Evaluation --------------------------------------------------------------
% policyloc_verbose : policyloc with explicit neighborhood state
%      cell array (1xn) containing a matrix |SNi|x(nb of neighbor +1)
% The first column of a matrix contains the actions preconised by 
% the policy for each neighborhood state.
% The last column(s) contains the state of the neighborhood as a vector. 


policyloc_verbose=[];
% Check arguments
n=length(policyloc);
if  ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: policyloc is not correct')
    disp('--------------------------------------------------------')
elseif (nargin > 2) &&  any(~ismember(sites, 1:n)) 
    disp('---------------------------------------------------------------')
    disp('GMDP Toolbox ERROR: sites must be a vector of sites in [1 n]')
    disp('---------------------------------------------------------------')
elseif (nargin > 3) && (isdisplayed~=true && isdisplayed~=false )
    disp('---------------------------------------------------------------')
    disp('GMDP Toolbox ERROR: isdisplayed must be a boolean')
    disp('---------------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
    
    % Set optional arguments
    if ~(nargin > 2); sites = 1:n; else sites = sort(unique(sites)); end
    if ~(nargin > 3); isdisplayed = true; end;
    
    policyloc_verbose = cell(1,n);
    for i = sites
        SN_i = size(GMDP.VN{i},1);
        policyloc_verbose{i} = zeros(SN_i, length(GMDP.N{i})+1); % first column is action
        policyloc_verbose{i}(:,1) = policyloc{i};
        for j=1:SN_i; policyloc_verbose{i}(j,2:end) = GMDP.VN{i}(j,:); end
        
        if isdisplayed
            disp('---------------------------------------------------------------');
            disp(['Site' , ' ' , int2str(i),'  ' , 'Neighbor(s)' , ': ' , int2str(GMDP.N{i})]);
            disp('---------------------------------------------------------------');
            disp(['Action',' | ','State',' | ','State of the neighbor(s)']);
            for j=1:SN_i
                disp([int2str(policyloc_verbose{i}(j,1)),'      | ', ...
                    int2str(j),'     | ' , int2str(policyloc_verbose{i}(j,2:end))])
            end
        end
    end
end


