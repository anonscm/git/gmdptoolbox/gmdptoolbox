function GMDP = gmdp_example_epidemio_circle(pepsilon, pc, pd, r,n)

% gmdp_example_epidemio    generate a Graph-based MDP (GMDP) example based  
%                          on the management of n sites located on a circle (site i is connected to sites i-1 and i+1, site 1 is connected to sites 2 and n) to maximize the reward 
%                          avoiding an epidemic
% 2 states for each site: sane(1), infected(2)
% 2 possible actions for each site: usual action(1), treatment(2)
% Arguments -------------------------------------------------------------
% pepsilon (optional) : probability of long distance contamination (default value: 0.2)
% pc (optional) : probability of contamination by a neighbor site (default value: 0.5)
% pd (optional) : probability of decontamination if treatment is performed (default value: 0.8)
% r (optional) : reward if state is sane and usual action is performed (default value: 4)
% n (optional) : Number of sites on the circle.
% Evaluation -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)


GMDP=[];
%% arguments checking
if (nargin > 0) && (~isnumeric(pepsilon) || pepsilon < 0 || pepsilon > 1)
  disp('----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: Probability pepsilon must be in [0; 1]')
  disp('----------------------------------------------------------')
elseif (nargin > 1) && (~isnumeric(pc) || pc < 0 || pc > 1)
  disp('-----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: Probability pc must be in [0; 1]')
  disp('-----------------------------------------------------------')
elseif (nargin > 2) && (~isnumeric(pd) || pd < 0 || pd > 1)
  disp('-----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: Probability pd must be in [0; 1]')
  disp('-----------------------------------------------------------')
elseif (nargin > 3) && ~isnumeric(r)
  disp('-----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: r must be a numerical reward')
  disp('-----------------------------------------------------------')
elseif (nargin > 4) && (~isnumeric(n) || n<3)
  disp('-----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: n must be an integer, larger or equal to 3')
  disp('-----------------------------------------------------------')

else

    % initialization of optional arguments
    if ~(nargin > 0); pepsilon = 0.2; end;
    if ~(nargin > 1); pc = 0.5; end;
    if ~(nargin > 2); pd = 0.8; end;
    if ~(nargin > 3); r=4; end;
    if ~(nargin > 4); n=3; end;    

    %% Defining the GMDP
    GMDP.M = 2*ones(1,n); % number of states in each site
    GMDP.B = 2*ones(1,n); % number of action in each site
    GMDP.G = full(gallery('tridiag',n,1,1,1));
    GMDP.G(1,n)=1;
    GMDP.G(n,1)=1
    % Local transition probabilities    
    n = length(GMDP.M);
    nni = 0:(n-1);
    pin = pepsilon + (1 - pepsilon)*(1 - (1 - pc).^nni);
    P = zeros(8,2,2);
    P(:,:,1) = [1 - pin(1)  pin(1);
                1 - pin(2)  pin(2); 
                1 - pin(2)   pin(2);
                1 - pin(3)   pin(3); 
                0           1;
                0           1;
                0           1;
                0           1];
     P(:,:,2) = [1          0;
                 1          0;
                 1          0;
                 1          0;
                 pd         1-pd;
                 pd         1-pd;
                 pd         1-pd;
                 pd         1-pd];
    GMDP.Ploc{1} = P;
    for i=2:(n-1)
        P = zeros(8,2,2);
        P(:,:,1) = [1 - pin(1)  pin(1);
                    1 - pin(2)  pin(2); 
                    0           1;
                    0           1;
                    1 - pin(2)   pin(2);
                    1 - pin(3)   pin(3); 
                    0           1;
                    0           1];
        P(:,:,2) = [1          0;
                    1          0;
                    pd         1-pd;
                    pd         1-pd;
                    1          0;
                    1          0;
                    pd         1-pd;
                    pd         1-pd];
        GMDP.Ploc{i} = P;
    end
    P = zeros(8,2,2);
    P(:,:,1) = [1 - pin(1)  pin(1);
                0           1;
                1 - pin(2)   pin(2);
                0           1;
                1 - pin(2)  pin(2); 
                0           1;
                1 - pin(3)   pin(3); 
                0           1];
    P(:,:,2) = [1          0;
                pd         1-pd;
                1          0;
                pd         1-pd;
                1          0;
                pd         1-pd;
                1          0;
                pd         1-pd];
    GMDP.Ploc{n} = P;

    % local rewards
    GMDP.Rloc{1} =     [r   0;
                        r   0;
                        r   0;
                        r   0;
                        r/2 0;
                        r/2 0;
                        r/2 0;
                        r/2 0];
    for i=2:(n-1)
        GMDP.Rloc{i} = [r   0;
                        r   0;
                        r/2 0;
                        r/2 0;
                        r   0;
                        r   0;
                        r/2 0;
                        r/2 0];
    end
    GMDP.Rloc{n} =     [r   0;
                        r/2 0;
                        r   0;
                        r/2 0;
                        r   0;
                        r/2 0;
                        r   0;
                        r/2 0];

    [~, ~, GMDP] = gmdp_check_gmdp(GMDP); % compute additional attributs
end
       
