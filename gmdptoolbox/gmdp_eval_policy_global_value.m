function V = gmdp_eval_policy_global_value (GMDP, Vloc)

% gmdp_eval_policy_global_value     Compute global value function from Vloc
% Arguments -------------------------------------------------------------
% Let n be the number of site, |Si| the number of state of site i
% |SNi| the number of state of the site i neighborhood
% GMDP : a structure describing the GMDP (see joint documentation)
% Vloc : local value function associated to the given policy
%        a cell array (1xn) containing at place i a vector (|SNi|x1)
% Evaluation -------------------------------------------------------------
% V : global value function associated to a given policy
%     a vector (prod(|Si|)x1)


V=[];
% check of arguments
n = length(GMDP.M);
isOKVloc = true; if length(Vloc)~=n; isOKVloc = false; end
for i=1:n
    if isOKVloc && ( length(Vloc{i})~=size(GMDP.VN{i},1) ); isOKVloc = false; end
end

if ~isOKVloc
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Vloc is not compatible with GMDP')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
 
    S = prod(GMDP.M);
    V = zeros(S,1);
    for s = 1:S
        v_s = itov(s,GMDP.M);
        for i=1:n
            v_sN = v_s(GMDP.N{i});
            sN = vtoi(v_sN, GMDP.M(GMDP.N{i}));
            V(s) = V(s) + Vloc{i}(sN);
        end
    end
end
