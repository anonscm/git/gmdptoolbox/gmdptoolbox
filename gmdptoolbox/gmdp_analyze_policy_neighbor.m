function actions_repartition_state_site = gmdp_analyze_policy_neighbor(GMDP, policyloc, sites)

% gmdp_analyze_policy_neighbor    Compute and graph actions repartition 
% for each state of each site considering the given policy 
%                                  
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a policy, cell array (1xn)
% sites (optional) : vector of sites to be displayed (default value: all sites)
% Evaluation --------------------------------------------------------------
% actions_repartition_state_site : cell array (1xn) that contains proportions of actions 
%                       for each state site considering the given policy.

% Examples : 
% GMDP= gmdp_example_epidemio; 
% actions_repartition_state_site = gmdp_analyze_policy_neighbor(GMDP, policyloc)
% actions_repartition_state_site = 
%    [3x2 double]    [3x2 double]    [3x2 double]
% GMDP= gmdp_example_rand(3,[3 4 5], [5 4 3]); 
% actions_repartition_state_site = gmdp_analyze_policy_neighbor(GMDP, policyloc,[2 3])
% actions_repartition_state_site = 
%   []    [5x4 double]    [6x3 double]
MAX_NB_SITES = 12; % maximum number of plot in a figure


actions_repartition_state_site = []; 
n = length(GMDP.M); % number of sites
% Check of arguments
if ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP toolbox ERROR: policyloc argument is not correct !')
    disp('--------------------------------------------------------')
elseif nargin>2 && any(~ismember(sites, 1:n))  
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: sites must be a vector of sites in [1 n]')
    disp('--------------------------------------------------------')
else        
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp(['GMDP Toolbox WARNING: Only the ' int2str(MAX_NB_SITES) 'th first sites are plot'])
    disp('--------------------------------------------------------')
    
    % intialzing of arguments
    if ~(nargin>2)
        sites = 1:n;
        sites_plots=1:min(n,MAX_NB_SITES); 
    else
        sites = unique(sites); 
        sites_plots=sort( sites(1:min(length(sites), MAX_NB_SITES)) );
    end

    % compute actions repartition
    actions_repartition_state_site = cell(1,n); 
    for k=1:length(sites)
        i=sites(k);
        actions_repartition_state_site{i} = zeros(GMDP.M(i)+1,GMDP.B(i));
        iN = find(GMDP.N{i}==i);
        SN_i = size(GMDP.VN{i},1);
        if isempty(iN) % site not neihghbor of itself
            sN_for_s = 1:SN_i;
            for j=1:GMDP.B(i)
                 r = sum(policyloc{i}(sN_for_s)==j)/length(sN_for_s);
                 actions_repartition_state_site{i}(:,j) = r;
            end
        else
            for s = 1:GMDP.M(i)    
                sN_for_s=[];
                for sN=1:SN_i
                    v_sN = GMDP.VN{i}(sN,:);
                    if v_sN(iN)==s; sN_for_s=[sN_for_s sN]; end
                end
                for j=1:GMDP.B(i)
                    r = sum(policyloc{i}(sN_for_s)==j);
                    actions_repartition_state_site{i}(s,j) = r/length(sN_for_s);
                end
            end
        end
        actions_repartition_state_site{i}(GMDP.M(i)+1,:) = sum(actions_repartition_state_site{i}(1:GMDP.M(i),:))/GMDP.M(i);
    end

    % plot graph (a subplot per site)
    nb_row_plot = ceil(length(sites_plots)/4); % 4 plots per row
    nb_col_plot = min(length(sites_plots),4);
    pos = [1 1 500*nb_col_plot  300*nb_row_plot]; % size of a plot near 0.7 size of a default plot
    h=figure('Position',pos);
    set(h,'Name','Repartition of action for each state of sites considering a given policy');
    for k=1:length(sites_plots)
        i=sites(k);
        subplot(nb_row_plot,nb_col_plot,k);
        bar(actions_repartition_state_site{i},'stack','BarWidth',0.6);
        xlabel('States');
        nb_bar = GMDP.B(i)+1;
        if nb_bar<=15 % if too many bar do not work
            xtickslabels =  {int2str((1:GMDP.M(i))'),'All'};
            set(gca,'XTickLabel', xtickslabels)
        end
        ylabel('Percentages per actions');
        legend_label = [repmat('Action ',GMDP.B(i),1)  int2str((1:GMDP.B(i))')];
        legend(legend_label,'location','EastOutside')
        title(['Actions repartition per states for site ' int2str(i) ' for a given policy']);
    end
end









