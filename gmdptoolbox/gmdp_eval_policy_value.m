function value_evolution = gmdp_eval_policy_value(discount, sim_reward, iscumulated)

% gmdp_eval_policy_value   Compute and graph global value evolution over time 
%                          after simulation of applying a given policy
% Arguments ---------------------------------------------------------------
% discount : discount rate in ]0; 1[ or 1 for no discount applied
% sim_reward : array (nb_simulation x nb_period x nn_site) of simulated evolution reward
% iscumulated (optional) : if true: compute cumulative global value, 
%                          if false: compute instantaneous global value
%                          (default value: true)
% Evaluation --------------------------------------------------------------
% value_evolution : vector (1xnb_period) of global (on all sites) value at each period
%                folowing input arguments: cumulated or not, discounted or not

value_evolution = [];
%Check of arguments
if  ~isnumeric(discount) || discount <= 0 || discount > 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: discount must be numeric in ]0,1]')
    disp('--------------------------------------------------------')
elseif length(size(sim_reward)) ~=3 || any(~isnumeric(sim_reward)) 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: sim_reward must be a numeric 3D array (nb_sim x nb_period x nb_site)')
    disp('--------------------------------------------------------') 
elseif (nargin>2) && (iscumulated~=true && iscumulated~=false)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: iscumulated argument must be a boolean')
    disp('--------------------------------------------------------')     
else 

    % set optional argument
    if ~(nargin>2); iscumulated=true; end
    
    nb_sim = size(sim_reward,1);
    nb_period = size(sim_reward,2);
    
    value_evolution = zeros(1,nb_period);
    % compute instantaneous global value
    for t=1:nb_period
        value_evolution(t) = sum(sum(sim_reward(:,t,:)))/nb_sim;
    end
    % consider discounted values
    amortization = 1:nb_period;
    value_evolution = value_evolution .* discount.^amortization;
    if iscumulated; value_evolution = cumsum(value_evolution); end
    
    h = figure; 
    plot(value_evolution);
    if iscumulated; value_name='Cumulative'; else value_name='Instantaneous';end
    set(h,'Name',[value_name ' global value evolution applying a given policy']);
    xlabel('Time period');    
    ylabel([value_name ' global value']);
    title({[value_name ' global value evolution applying'] ['a given policy with a discount rate of ' num2str(discount)]});
end
