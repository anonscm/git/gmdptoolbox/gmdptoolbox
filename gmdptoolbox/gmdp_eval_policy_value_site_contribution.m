function value_site = gmdp_eval_policy_value_site_contribution(GMDP, discount, sim_reward)

% gmdp_eval_policy_value    Estimate contribution of sites to the global value
%                          after simulation of applying a given policy
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% discount : discount rate in ]0; 1[ or 1 for no discount applied
% sim_reward :  array ((b_simulation x nb_period x nb_site) of simulated evolution reward
% Evaluation --------------------------------------------------------------
% value_period : vector (1xnb\_site) of contribution of each site at (discounted or not) value


value_site=[];
%Check of arguments
n = length(GMDP.M); % number of site
if ~isnumeric(discount) || discount <= 0 || discount > 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: discount must be numeric in ]0,1]')
    disp('--------------------------------------------------------')
elseif length(size(sim_reward)) ~=3 || ~isnumeric(sim_reward) || size(sim_reward,3)~=n
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: sim_reward must be a numeric 3D array (nb_sim x nb_period x nb_site)')
    disp('--------------------------------------------------------')   
else 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')

    nb_sim = size(sim_reward,1);
    nb_period = size(sim_reward,2);
    
    % compute percentage of time spend in each state of sites
    value_site = zeros(1,n);
    amortization = 1:nb_period;
   for i=1:n
        value_period = zeros(1,nb_period);
        for t=1:nb_period
            value_period = sum(sim_reward(:,t,i))/nb_sim;
        end
        value_period = cumsum(value_period .* discount.^amortization);
        value_site(i) = value_period(end);
    end
    value_site = 100*value_site/sum(value_site); % normalize percentage

    % plot graph
    % Define a colormap (jet - default Matlab colormap)
    c=[ 0         0    0.5625
        0         0    0.6250
        0         0    0.6875
        0         0    0.7500
        0         0    0.8125
        0         0    0.8750
        0         0    0.9375
        0         0    1.0000
        0    0.0625    1.0000
        0    0.1250    1.0000
        0    0.1875    1.0000
        0    0.2500    1.0000
        0    0.3125    1.0000
        0    0.3750    1.0000
        0    0.4375    1.0000
        0    0.5000    1.0000
        0    0.5625    1.0000
        0    0.6250    1.0000
        0    0.6875    1.0000
        0    0.7500    1.0000
        0    0.8125    1.0000
        0    0.8750    1.0000
        0    0.9375    1.0000
        0    1.0000    1.0000
        0.0625    1.0000    0.9375
        0.1250    1.0000    0.8750
        0.1875    1.0000    0.8125
        0.2500    1.0000    0.7500
        0.3125    1.0000    0.6875
        0.3750    1.0000    0.6250
        0.4375    1.0000    0.5625
        0.5000    1.0000    0.5000
        0.5625    1.0000    0.4375
        0.6250    1.0000    0.3750
        0.6875    1.0000    0.3125
        0.7500    1.0000    0.2500
        0.8125    1.0000    0.1875
        0.8750    1.0000    0.1250
        0.9375    1.0000    0.0625
        1.0000    1.0000         0
        1.0000    0.9375         0
        1.0000    0.8750         0
        1.0000    0.8125         0
        1.0000    0.7500         0
        1.0000    0.6875         0
        1.0000    0.6250         0
        1.0000    0.5625         0
        1.0000    0.5000         0
        1.0000    0.4375         0
        1.0000    0.3750         0
        1.0000    0.3125         0
        1.0000    0.2500         0
        1.0000    0.1875         0
        1.0000    0.1250         0
        1.0000    0.0625         0
        1.0000         0         0
        0.9375         0         0
        0.8750         0         0
        0.8125         0         0
        0.7500         0         0
        0.6875         0         0
        0.6250         0         0
        0.5625         0         0
        0.5000         0         0
        ];
    nbcolors = size(c,1); % number of colors in the map
    nodeName = cell(n,1);
    nodeDescription = cell(n,1); % vector of descriptions (site, state, action)
    nodeColor=cell(n,1);
    for i=1:n
        nodeName{i} = num2str(value_site(i),'%.1f');
        nodeDescription{i}={['Site ' int2str(i)]};
        nodeColor{i}=c(round( value_site(i)*(nbcolors-1)/100 + 1 ),:);
   end
    [~,h]=evalc('graphViz4Matlab(GMDP.G,''-nodeLabels'',nodeName,''-nodeColors'',nodeColor,''-nodeDescriptions'',nodeDescription)');
    set(h.fig,'Name','Contribution of sites to the global value applying a giben policy');
    % Sites not neighbors with themselves are circled with dot line
    for i = find(diag(GMDP.G)==0)
        set(h.nodeArray(i),'lineStyle',':','lineWidth',0.01);
    end
end




