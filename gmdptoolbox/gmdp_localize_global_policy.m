function [islocal policyloc] = gmdp_localize_global_policy(GMDP, policy)

% gmdp_localize_global_policy  If it exists, computes the local policy from a global policy
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policy : the corresponding global policy, vector (prod(GMDP.M)x1)
% Evaluation --------------------------------------------------------------
% islocal : boolean indicating if a local policy exists
% policyloc : a local policy, cell array (1xn)

% Example: 
% GMDP= gmdp_example_epidemio; 
% policyloc = gmdp_linear_programming (GMDP, 0.95);
% [islocal policyloc] = gmdp_localize_global_policy(GMDP, policy)
% islocal =
%     1
% policyloc = 
%    [4x1 double]    [8x1 double]    [4x1 double]

policyloc = []; islocal=false;
% Check of arguments
n = length(GMDP.M); % number of sites
S = prod(GMDP.M); % number of global states
A = prod(GMDP.B); % number of global actions
if ~all(size(policy)==[S 1]) || any(~isnumeric(policy)) || any(~ismember(policy,1:A))
    disp('--------------------------------------------------------')
    disp('GMDP toolbox ERROR: policy argument is not correct !')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
    
    policyloc = cell(1,n); for i=1:n; policyloc{i}=zeros(size(GMDP.VN{i},1),1);end
    islocal=true;
    for s = 1:S
        if islocal
            sloc = itov(s, GMDP.M);
            aloc = itov(policy(s), GMDP.B);
            for i = 1:n % for each site
                if islocal
                    ineighbors = GMDP.N{i};
                    iloc = vtoi( sloc(ineighbors), GMDP.M(ineighbors) );
                    if policyloc{i}(iloc)==0; policyloc{i}(iloc)=aloc(i);
                    elseif policyloc{i}(iloc)~=aloc(i); islocal = false;
                    end
                end
            end
        end
    end
    if ~islocal; policyloc=[];end
end

    
