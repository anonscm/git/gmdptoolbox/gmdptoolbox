function policyloc = gmdp_linear_programming (GMDP, discount, options)

% gmdp_linear_programming  Resolution of discounted Grah-based MDP 
%                           with approximated linear programming
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% discount : discount rate in ]0; 1[
% options (optional) : options for calling linprog (default value: [])
% Evaluation --------------------------------------------------------------
% policyloc : approximated optimum policy


policyloc=[];
% check of arguments
if ~isnumeric(discount) || discount <= 0 || discount >= 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: discount argument must be in ]0,1[')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    if (nargin>2);disp('GMDP Toolbox WARNING: options argument is not checked by the function !');end
    
    if ~(nargin>2); options = []; end
   
    n = length(GMDP.M);
    
    pbsites = [];
    Phi = zeros(n,1);
    policyloc=cell(1,n);
    parfor i=1:n
        disp (['Resolve site ' int2str(i)])
        % resolve a LP problem for each site, min f'x such that Ax <= b
        % Dimensions A : (2*SN(i)*B(i) x (1+M(i)), b : (2*SN(i)*B(i) x 1
        %          |-1   M|         |  Rloc{i}(sN,a) |
        % with A = |-1  -M|     b = | -Rloc{i}(sN,a) |
        SN_i = size(GMDP.VN{i},1);
        w = zeros(GMDP.M(i),1);
        f = [1; w];
        M = zeros(SN_i, GMDP.M(i));
        bb = zeros(SN_i*GMDP.B(i),1);
        for ss = 1:GMDP.M(i)
             for sN=1:SN_i
                h=0; v_sN = GMDP.VN{i}(sN,:); if v_sN(GMDP.N{i}==i)==ss; h=1; end
                for a = 1:GMDP.B(i)
                    sLP = vtoi( [a sN], [GMDP.B(i) SN_i] );
                    M(sLP, ss) = h - discount*GMDP.Ploc{i}(sN,ss,a);
                    bb(sLP) = GMDP.Rloc{i}(sN,a);
               end
            end
        end
        A = [-ones(2*SN_i*GMDP.B(i),1) [M; -M]];
        b = [bb; -bb];
        [x,~,exitflag] = linprog(f, A, b,[],[],[],[],[],options); 
        if exitflag ~=1;
            if exitflag == 0; disp(['Site ' int2str(i) ': Number of iterations exceeded options.MaxIter.']);
            elseif exitflag == -2; disp(['Site ' int2str(i) ': No feasible point was found.']);
            elseif exitflag == -3; disp(['Site ' int2str(i) ': Problem is unbounded.']);
            elseif exitflag == -4; disp(['Site ' int2str(i) ': NaN value was encountered during execution of the algorithm.']);
            elseif exitflag == -5; disp(['Site ' int2str(i) ': Both primal and dual problems are infeasible.']);
            elseif exitflag == -7; disp(['Site ' int2str(i) ': Search direction became too small. No further progress could be made.']);
            end
        end
        if isempty(x)
            pbsites = [pbsites i];
            [~,pol] = max(GMDP.Rloc{i}');policyloc{i}=pol';
        else
            Phi(i) = x(1);
            w = x(2:end);
            
            % define site policy
            policyloc{i}=zeros(SN_i,1);
            for sN=1:SN_i
                % compute Ci(ai, sNi)
                C = GMDP.Rloc{i}(sN,:);
                for a = 1:GMDP.B(i)
                    Z1 = 0;
                    for ss = 1:GMDP.M(i)
                        Z1 = Z1 + GMDP.Ploc{i}(sN,ss,a)*w(ss);
                    end
                    C(a) = C(a) + discount*Z1;
                end
                [~,policyloc{i}(sN)] = max(C);
            end
        end
        disp (['Done site ' int2str(i)])
    end
    if ~isempty(pbsites)
        disp(['GMDP Toolbox WARNING: rough approximation of the policy for site(s): ' int2str(pbsites)])
    end
    disp('--------------------------------------------------------')
end
