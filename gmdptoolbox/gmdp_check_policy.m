function [isOK, problems] = gmdp_check_policy(GMDP, policyloc, ischecked)

% gmdp_check_policy    Check if policy cell array is valid
% Arguments -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a policy, cell array (1xn)
% ischecked (optional) : if true, check GMDP ; otherwise, do not check GMDP
%                        (default value: true)
% Evaluation -------------------------------------------------------------
% isOK : true if GMDP is valid, false otherwise
% problems : cell array of problems messages

k = 0; problems=[];
% Check arguments
if (nargin > 2) && ( ischecked~=true && ischecked~=false )
    disp('---------------------------------------------------------------')
    disp('GMDP Toolbox ERROR: ischecked argument must be a boolean')
    disp('---------------------------------------------------------------')
else
    % Set optional arguments
    if ~(nargin > 2); ischecked=true; end;
    % n : number of site
    n = size(GMDP.M,2);
    
    % Check GMDP
    if ischecked
        [~, problems] = gmdp_check_gmdp(GMDP);
        k = length(problems);
    end
    
    % Check policyloc
    pbsites = [];
    for i=1:n
        SN_i = size(GMDP.VN{i},1);  % number of states for the neighbors
        if  length(policyloc{i}) ~= SN_i || ~isnumeric(policyloc{i}) ...
                || any(~ismember(policyloc{i},1:GMDP.B(i)))
            pbsites = [pbsites i];
        end
    end
    if ~isempty(pbsites)
        k=k+1;problems{k} = ['GMDP Toolbox ERROR: policyloc is not valid for site(s) ' int2str(pbsites)];
    end
end

if k==0; isOK=true; else isOK=false; end





       
  

