function GMDP = gmdp_example_grid(n)

% gmdp_example_grid    generate a Graph-based MDP (GMDP) example 
%                      with a grid nxn for neighbor sites relations
% Sites are coded as follow:
%     1    ...  n
%     n+1      2*n
%      .        .
%     n*n-1 ... n*n
% With this layout, sites neighborhood relations draw a grid.
% Each site as 2 possible states. Sites may be fields with 2 states: sane(1), infected(2).
% Each site as 2 possible actions. Actions may be: usual production action(1), treatment again disease(2).
% Transition probabilities and rewards are randomly set.
% Arguments -------------------------------------------------------------
% n : number of sites on a side of square grid graph of all sites spatial relations
% Evaluation -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)


GMDP=[];
%% arguments checking
if ~isnumeric(n) || floor(n) ~= n || n < 2
  disp('----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: n must be an integer greater than 1')
  disp('----------------------------------------------------------')
else
   
    %% Defining the GMDP
    M = 2*ones(1,n*n); % number of states in each site
    B = 2*ones(1,n*n); % number of action in each site
    G = zeros(n,n);
    top_line = 2:(n-1);
    bottom_line = (n*n-n+2):n*n-1;
    left_column = []; for i=(n+1):n:((n-1)*n); left_column = [left_column i]; end
    right_column = []; for i=(2*n):n:((n-1)*n); right_column = [right_column i]; end
    for i=1:n*n
        if i == 1 % a corner
            sites = [1, 2, n+1];
        elseif i == n % a corner
            sites = [n-1, n, 2*n];
        elseif i == n*n-n+1 % a corner
            sites = [i-n, i, i+1];
        elseif i == n*n % a corner
            sites = [i, i-1, i-n];
        elseif  ismember(i,top_line) % top line
            sites = [i, i-1, i+1, i+n];
        elseif  ismember(i,bottom_line) % last line
            sites = [i, i-1, i+1, i-n];
        elseif  ismember(i,left_column) % left column
            sites = [i, i+n, i+1, i-n];
        elseif  ismember(i,right_column) % rigth column
            sites = [i, i+n, i-1, i-n];
        else
            sites = [i, i+n, i+1,i-1 ,i-n];
        end
        G (sites,i)=1;
    end
    GMDP = gmdp_example_rand(n*n, M, B, n*n, G);

    [~, ~, GMDP] = gmdp_check_gmdp(GMDP); % compute additional attributs
end
       
