function policy = gmdp_globalize_local_policy(GMDP, policyloc)

% gmdp_globalize_local_policy  Computes the global policy from a local policy
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a local policy, cell array (1xn)
% Evaluation --------------------------------------------------------------
% policy : the corresponding global policy, vector (prod(GMDP.M)x1)

% Example: 
% GMDP= gmdp_example_epidemio; 
% policyloc = gmdp_linear_programming (GMDP, 0.95);
% policy = gmdp_globalize_local_policy(GMDP, policyloc)
% policy =
%     1     2     3     4     5     6     7     8

policy = []; 
% Check of arguments
n=length(GMDP.M); % number of sites
if ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP toolbox ERROR: policyloc argument is not correct !')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
    
    n = length(GMDP.M); % number of sites
    S = prod(GMDP.M); % number of global states
    
    policy = zeros(S,1);
    for s = 1:S
        aloc = zeros(1,n);
        sloc = itov(s, GMDP.M);
        for i = 1:n % for each site
            ineighbors = find(GMDP.G(:,i))';
            iloc = vtoi( sloc(ineighbors), GMDP.M(ineighbors) );
            a(i) = policyloc{i}(iloc);
        end
        policy(s) = vtoi(a, GMDP.M);
    end
end


