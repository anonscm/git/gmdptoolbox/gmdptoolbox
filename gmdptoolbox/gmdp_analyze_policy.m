function actions_repartition = gmdp_analyze_policy(GMDP, policyloc, sites)

% gmdp_analyze_policy  Compute and graph actions repartition for each site 
%                      considering a given policy
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a policy, cell array (1xn)
% sites (optional) : vector of sites to be considered (default value: all sites)
% Evaluation --------------------------------------------------------------
% actions_repartition : cell array (1x(n+1)) that contains proportions of actions 
%                       for each site and for all sites considering the given policy.
% If sites have not the same number of possible actions or all sites are not 
% considered, the last cell (corresponding to all sites) is empty.

% Examples : 
% GMDP= gmdp_example_epidemio; policyloc = gmdp_linear_programming (GMDP, 0.95);
% actions_repartition = gmdp_analyze_policy(GMDP, policyloc)
% actions_repartition = 
%    [1x2 double]    [1x2 double]    [1x2 double]    [1x2 double]
% rng(0); GMDP= gmdp_example_rand(3,[3 4 5], [5 4 3]); policyloc = gmdp_linear_programming (GMDP, 0.95);
% actions_repartition = gmdp_analyze_policy(GMDP, policyloc,[2 3])
% actions_repartition = 
%    []    [1x4 double]    [1x3 double]    []


actions_repartition = []; 
% Check of arguments
n=length(GMDP.M); % number of sites
if ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP toolbox ERROR: policyloc argument is not correct !')
    disp('--------------------------------------------------------')
elseif nargin>2 && any(~ismember(sites, 1:n))
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: sites must be a vector of sites in [1 n]')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
    
    %%initializing of arguments
    if ~(nargin>2); sites=1:n; else sites = sort(unique(sites)); end
    
    % compute actions repartition, if all sites have the same number of 
    % feasible actions, compute also repartition globally
    actions_repartition = cell(1,n+1); 
    if all(GMDP.B == GMDP.B(1)) && length(sites)==n
        isall = true; 
        actions_repartition{n+1} = zeros(1,GMDP.B(1));
    else
        isall = false; 
    end
    for i=sites
        SN = length(policyloc{i}); % number of state of the neighborhood
        actions_repartition{i} = zeros(1,GMDP.B(i));
        for a=1:GMDP.B(i)
            r = sum(policyloc{i}==a)/SN;
            actions_repartition{i}(a) = r; 
            if isall; actions_repartition{n+1}(a) = actions_repartition{n+1}(a) + r/n; end
        end
    end
    
    % plot graph (to have the same bar graph when only 1 site is considered,
    % the bar 'All' is always plot, even when there is no data)
    nb_bar = length(sites)+1; 
    data = zeros(nb_bar,max(GMDP.B));
    for k=1:length(sites); data(k, 1:length(actions_repartition{sites(k)})) = actions_repartition{sites(k)}; end
    if isall; data(nb_bar, 1:length(actions_repartition{n+1})) = actions_repartition{n+1}; end
    h = figure; 
    set(h,'Name','Repartition of action for each site considering a given policy');
    bar(data, 'stack', 'BarWidth', 0.6);
    xlabel('Sites');
    if nb_bar<=15 % if too many bar do not work
        xtickslabels =  {int2str(sites'),'All'};
        set(gca,'XTickLabel', xtickslabels)
    end
    ylabel('Percentages per actions');
    nb_max_action = max(GMDP.B(sites));
    legend_label = [repmat('Action ',nb_max_action,1)  int2str((1:nb_max_action)')];
    legend(legend_label,'location','EastOutside')
    title('Actions repartition for a given policy (here last site correspond to all sites)');
end


