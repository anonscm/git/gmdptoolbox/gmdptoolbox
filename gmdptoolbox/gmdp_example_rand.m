function GMDP = gmdp_example_rand(n, M, B, maxN, G)

% gmdp_example_rand    generate a random Graph-based MDP (GMDP) example

% Arguments -------------------------------------------------------------
% n (optional) : number of site (default value: 3)
% M (optional) : vector (1xn) of the number of state for each site
%     (by default 2 states by site)
% B (optional) : vector (1xn) of the number of action for each site
%     (by default 2 actions by site)
% maxN (optional) : maximum number of neighbor for a site
%     (by default min(n,4) )
% G (optional) : adjacency matrix describing the neighborhood of each site
%     (by default a random matrix)
% Evaluation -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)


GMDP = []; 
%% arguments checking
if (nargin > 0) && (~isnumeric(n) || n<1 || n~=floor(n))
    disp('----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: n (the number of site) must be an integer upper than 1')
    disp('----------------------------------------------------------')
elseif (nargin > 1) && (~isnumeric(M) || length(M)~=n || any(M<2) || any(M~=floor(M)))
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: M (number of state for each site) must be a vector 1xn of integer upper than 1')
    disp('-----------------------------------------------------------')
elseif (nargin > 2) && (~isnumeric(B) || length(B)~=n || any(B<2) || any(B~=floor(B)))
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: B (number of action for each site) must be a vector 1xn of integer upper than 1')
    disp('-----------------------------------------------------------')
elseif (nargin > 3) && (~isnumeric(maxN) || maxN<=0 || maxN~=floor(maxN) || maxN>n)
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: maxN (maximum number of neighbor for a site) must be an integer in [1, n]')
    disp('-----------------------------------------------------------')
elseif (nargin > 4) && (~isnumeric(G) || any(size(G)~=[n n]) || any(any(G~=0 & G~=1)) || ~is_graph_connected(G) || max(sum(G))>maxN)
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: G (sites neighborhood) must be a matrix nxn containing only 0 and 1, representing a connected graph with less than maxN neighbor per site')
    disp('-----------------------------------------------------------')
else
    
    % initialization of optional arguments
    if ~(nargin > 0); n=3; end;
    if ~(nargin > 1); M=2*ones(1,n); end;
    if ~(nargin > 2); B=2*ones(1,n); end;
    if ~(nargin > 3); maxN=min(n,4); end;
    if ~(nargin > 4); G=generate_random_graph(n,maxN); end
    
    if size(G,1)~=0
        %% Defining the GMDP
        GMDP.M = M; % number of states in each site
        GMDP.B = B; % number of action in each site
        GMDP.G = G; % adjacency matrix
        
        for i=1:n
            N{i} = find(GMDP.G(:,i))'; % vector of neighbors
            SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
        end
        
        for i=1:n
            % Local transition probabilities
            GMDP.Ploc{i} = zeros(SN(i),GMDP.M(i),GMDP.B(i));
            for a=1:GMDP.B(i)
                GMDP.Ploc{i}(:,:,a)=rand(SN(i),GMDP.M(i));
                GMDP.Ploc{i}(:,:,a)=GMDP.Ploc{i}(:,:,a)./repmat(sum(GMDP.Ploc{i}(:,:,a)')',1,GMDP.M(i));
            end
            
            % Local reward
            GMDP.Rloc{i} = rand(SN(i),GMDP.B(i));
        end

        [~, ~, GMDP] = gmdp_check_gmdp(GMDP); % compute additional attributs
    end
end

    

