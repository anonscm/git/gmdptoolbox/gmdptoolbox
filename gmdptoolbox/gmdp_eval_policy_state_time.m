function state_time = gmdp_eval_policy_state_time(GMDP, sim_state, sites)

% gmdp_eval_policy_state_time   Compute and graph time spent in each state for sites 
%                               after simulation of applying a given policy
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% sim_state : array (nb_simultion x nb_period x nb_site) of simulated evolution states
% sites (optional) : vector of sites to be displayed (default value: all sites)
% Evaluation --------------------------------------------------------------
% state_time : cell array (1x(n+1)) containing vector of
%              percentage of time spend in each possible state of sites
% The last cell element called 'All' is computed only if all sites have
% the same number of states and if argument sites defined all sites.

% Examples : 
% GMDP= gmdp_example_epidemio; 
% state_time = gmdp_eval_policy_state_time(GMDP, sim_state)
%state_time = 
%    [2x1 double]    [2x1 double]    [2x1 double]    [2x1 double]
% state_time = gmdp_eval_policy_state_time(GMDP, sim_state, [2 3])
% state_time = 
%    []    [2x1 double]    [2x1 double]    []   []


state_time=[];
% Check of arguments
n = length(GMDP.G); % number of site
if length(size(sim_state)) ~=3 || ~isnumeric(sim_state) || size(sim_state,3)~=n 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: sim_state must be a numeric 3D array (nb_sim x nb_period x nb_site)')
    disp('--------------------------------------------------------')   
elseif nargin>2 && any(~ismember(sites, 1:n))
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: sites must be a vector of sites in [1 n]')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
    
    % intialization of optional argument
    if ~(nargin>2); sites=1:n; else sites=sort(unique(sites)); end;

    nb_sim = size(sim_state,1);
    nb_period = size(sim_state,2);
    
    % compute average percentage time, if all sites have the same number of 
    % possible states and all sites are considered, compute also for all sites
    state_time = cell(1,n+1);
    if all(GMDP.M == GMDP.M(1)) && length(sites)==n
        isall = true; 
        state_time{n+1} = zeros(GMDP.M(1),1);
    else
        isall = false; 
    end

    for i=sites
        state_time{i} = zeros(GMDP.M(i),1);
        for s=1:GMDP.M(i)
            t = length(find(sim_state(:,:,i)==s))/(nb_sim*nb_period);
            state_time{i}(s) = t;
            if isall; state_time{n+1}(s) = state_time{n+1}(s) +t/n;
            end
        end
    end
    
    % plot graph (to have the same bar graph when only 1 site is considered,
    % the bar 'All' is always plot, even when there is no data)
    nb_bar = length(sites)+1; 
    nb_max_state = max(GMDP.M(sites));
    data = zeros(nb_bar, nb_max_state);
    for k=1:length(sites); i=sites(k); data(k,1:length(state_time{i})) = state_time{i}; end
    if isall; data(nb_bar,:) = state_time{n+1}; end
    h = figure;
    set(h,'Name','Repartition of state occupancy applying a given policy');
    bar(data, 'stack', 'BarWidth', 0.6);
    xlabel('Sites');
    if nb_bar<=15 % if too many bar do not work
        xtickslabels =  {int2str(sites'),'All'};
        set(gca,'XTickLabel', xtickslabels);
    end
    ylabel('Percentage per state');
    legend_label=[repmat('State ',nb_max_state,1)  int2str((1:nb_max_state)')];
    legend(legend_label,'location','EastOutside')
    title('Repartition of state occupancy applying a given policy');
end




