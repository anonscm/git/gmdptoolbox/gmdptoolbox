function gmdp_see_policy(GMDP, policyloc)

% gmdp_see_policy   Display actions of a policy
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a policy, cell array (1xn)
% Evaluation --------------------------------------------------------------
% none


% check of arguments
if  ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: policyloc is not correct')
    disp('--------------------------------------------------------')    
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')

    % just display policy actions
    for i=1:length(policyloc)
        disp('---------------------------------------------------------------');
        disp(['Site ' , int2str(i) , '  Neighbor(s): ', int2str(GMDP.N{i})]);
        disp(['Actions:' , ' ' , int2str(policyloc{i}')]);
    end
end



