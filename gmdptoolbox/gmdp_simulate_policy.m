function [sim_state, sim_reward, sim_action] = gmdp_simulate_policy(GMDP, policyloc, nb_init, nb_run, nb_period, state_init)

% gmdp_simulate    Simulate the application of a policy
% Arguments -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a policy, cell array (1 x n)
% nb_init (optional) : number of initial states to consider (default value: 100)
% nb_run (optional) : number of run for an initial state (default value: 100)
% nb_period (optional) : number of period to do (default value: 40)
% state_init (optional) : initial state, taken into account if nb_init=1 (défault value: [])
% Evaluation -------------------------------------------------------------
% sim_state : array (nb_simulation x nb_period x nb_site) of simulated evolution states
% sim_reward : array (nb_simulation x nb_period x nb_site) of simulated evolution reward
% sim_action : array (nb_simulation x nb_period x nb_site) of simulated evolution action

sim_state=[];
sim_reward=[];
% check of arguments
n = length(GMDP.M);
if ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: policyloc is not correct')
    disp('--------------------------------------------------------')
elseif (nargin > 2) && (~isnumeric(nb_init) ...
           || floor(nb_init)~=nb_init || nb_init < 1)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: nb_init must be an integer upper than 0')
    disp('--------------------------------------------------------')
elseif (nargin > 3) && (~isnumeric(nb_run) ...
           || floor(nb_run)~=nb_run || nb_run < 1)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: nb_run must be an integer upper than 0')
    disp('--------------------------------------------------------')
elseif (nargin > 4) && ( ~isnumeric(nb_period) ...
           || floor(nb_period)~=nb_period || nb_period < 1)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: nb_period must be an integer upper than 0')
    disp('--------------------------------------------------------')
 elseif (nargin > 5) && ( any(~isnumeric(state_init)) ...
           || any(floor(state_init)~=state_init) ...
           || any(state_init<1) || any(state_init>GMDP.M) ...
           || nb_init~=1)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: state_init must be compatible with GMDP.M and nb_init must be 1')
    disp('--------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
    
    % initialization of optional arguments 
    if ~(nargin > 2); nb_init = 100; end
    if ~(nargin > 3); nb_run = 100; end
    if ~(nargin > 4); nb_period = 40; end   
    if ~(nargin > 5); state_init = [];end
    
    sim_state = zeros(nb_init*nb_run, nb_period, n);  
    sim_reward = zeros(nb_init*nb_run, nb_period, n);  
    sim_action = zeros(nb_init*nb_run, nb_period, n);
    
    for i=1:nb_init
        for j=1:nb_run
            sim = (i-1)*nb_run + j;
            if isempty(state_init);
                v_s = []; for t=1:n; v_s = [v_s randi(GMDP.M(t))]; end
            else
                v_s = state_init;
            end
            for t=1:nb_period
                sim_state(sim,t,:) = v_s;
                v_ss = zeros(1,n); % state at the next period
                for k = 1:n
                    sN = vtoi(v_s(GMDP.N{k}), GMDP.M(GMDP.N{k})); % state of k neighborhood
                    a = policyloc{k}(sN);
                    sim_action(sim,t,k) = a;
                    sim_reward(sim,t,k) = GMDP.Rloc{k}(sN, a);
                    v_ss(k) = find( rand(1) < cumsum(GMDP.Ploc{k}(sN,:,a)), 1, 'first');
                end
                v_s = v_ss;
            end
        end
    end
end




       
  

