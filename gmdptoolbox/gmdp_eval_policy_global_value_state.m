function v = gmdp_eval_policy_global_value_state (GMDP, Vloc, v_s)

% gmdp_eval_policy_global_value_state     Compute global value function from Vloc 
%                             for a global state
% Arguments -------------------------------------------------------------
% Let n be the number of site, |Si| the number of state of site i
% |SNi| the number of state of the site i neighborhood
% GMDP : a structure describing the GMDP (see joint documentation)
% Vloc : local value function associated to the given policy
%        a cell array (1xn) containing at place i a vector (|SNi|x1)
% v_s : a global state, vector (1xn)
% Evaluation -------------------------------------------------------------
% v : global value corresponding to the given global state


v=[];
% check of arguments
n = length(GMDP.M);
isOKVloc = true; if length(Vloc)~=n; isOKVloc = false; end
for i=1:n
    if isOKVloc && ( length(Vloc{i})~=size(GMDP.VN{i},1) ); isOKVloc = false; end
end

if ~isOKVloc
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Vloc is not compatible with GMDP')
    disp('--------------------------------------------------------')
elseif ~isnumeric(v_s) || any(size(v_s)~=[1 n]) || any(v_s<1) || ...
        any(v_s>GMDP.M) || any(v_s~=floor(v_s))
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: v_s is not compatible with GMDP')
    disp('--------------------------------------------------------')   
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')
 
    v = 0;
    for i=1:n
        v_sN = v_s(GMDP.N{i});
        sN = vtoi(v_sN, GMDP.M(GMDP.N{i}));
        v = v + Vloc{i}(sN);
    end
end
