function Vloc = gmdp_eval_policy_MF (GMDP, discount, policyloc, Qloc0, Tend)

% gmdp_eval_policy_MF     Policy evaluation using Mean-Field approximation 
%                         of the transition probabilities
% Arguments -------------------------------------------------------------
% Let n be the number of site, |Si| the number of state of site i
% |SNi| the number of state of the site i neighborhood
% GMDP : a structure describing the GMDP (see joint documentation)
% discount  : discount rate in ]0; 1[
% policyloc : a policy, cell array (1xn), containing at place i a vector (|SNi|x1)
% Qloc0 (optional) : initial approximated probability, cell array (1xn) 
%                    containing at place i a vector (|Si|x1)
%                    (default value: uniform distribution)
% Tend (optional) : approximation of infinity (default value: 100)
% Evaluation -------------------------------------------------------------
% Vloc : approximated value function associated to the given policy
%        a cell array (1xn) containing at place i a vector (|SNi|x1)

threshold = 0.000001; % for numerical comparison

Vloc=[];
% check of arguments
n = length(GMDP.M);
isOKQloc0 = true; if (nargin > 3) && (length(Qloc0)~=n); isOKQloc0 = false; end
for i=1:n
    if (nargin > 3) && isOKQloc0 && ( length(Qloc0{i})~=GMDP.M(i) || ...
            any(Qloc0{i} <0) || any(Qloc0{i} >1) || abs(sum(Qloc0{i})-1)>threshold ); 
        isOKQloc0 = false; 
    end
end

if ~isnumeric(discount) || discount <= 0 || discount >= 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: discount argument must be in ]0,1[')
    disp('--------------------------------------------------------')
elseif ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: policyloc is not correct')
    disp('--------------------------------------------------------')
elseif ~isOKQloc0
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Qloc0 is not correct')
    disp('--------------------------------------------------------')
elseif (nargin > 4) && ( ~isnumeric(Tend) || Tend < 1 || floor(Tend)~=Tend ) 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Tend must be an integer upper than 0')
    disp('--------------------------------------------------------')
else

    % initialization of optional arguments 
    if ~(nargin > 3); for i=1:n; Qloc0{i}=(ones(GMDP.M(i),1)/GMDP.M(i));end;end
    if ~(nargin > 4); Tend = 100; end

    % Initialization
    Qmarg_prev = Qloc0;
    % no need of Qcond_prev for t=1
    Vloc = cell(1,n); SN = zeros(n,1);
    for i=1:n
        SN(i) = size(GMDP.VN{i},1);
        % init Vloc and Ploc policy
        Vloc{i} = zeros(SN(i),1);
        Plocpolicy{i} = zeros(SN(i),GMDP.M(i));
        for a=1:GMDP.B(i)
            ind = find(policyloc{i}==a);
            Vloc{i}(ind) = GMDP.Rloc{i}(ind,a);
            Plocpolicy{i}(ind,:) = GMDP.Ploc{i}(ind,:,a);
        end
   
        % compute for each si the sNi compatible with si
        c =  find(GMDP.N{i}==i) ; % column of site i in VN
        if ~isempty(c)
            equi_sN{i}=zeros(GMDP.M(i),SN(i)/GMDP.M(i));
            for s=1:GMDP.M(i)
                equi_sN{i}(s,:) = find(GMDP.VN{i}(:, c) == s)';
            end
        else
            % i is not itself a neighbor, all sNi states are then compatible
            % with each state of the site
            equi_sN{i}=repmat(1:SN(i),GMDP.M(i),1);
        end
    end
    
    t = 1;
    while t <= Tend
        % Compute approximate probabilities
        for i=1:n  % for each site

            % Qtrans
            if GMDP.G(i,i) == 0 % site not itself a neignbor
                Qt = sum(Plocpolicy{i},1)/SN(i); % mean on all neighbor states
                Qtrans{i} = repmat(Qt,GMDP.M(i),1); % same proba for each state of the site
            else
                Qtrans{i} = zeros(GMDP.M(i),GMDP.M(i)); % initialisation
                for s = 1:GMDP.M(i) % for all s_i
                    for sprev = 1:GMDP.M(i) % for all sprev_i
                        for sNprev = equi_sN{i}(sprev,:)
                            Z1 = 1;
                            for ln = 1:length(GMDP.N{i})
                                kn = GMDP.N{i}(ln); % a neighbor site
                                if kn ~= i;
                                    Z1=Z1*Qmarg_prev{kn}( GMDP.VN{i}(sNprev,ln) );
                                end
                            end
                            Qtrans{i}(sprev,s) = Qtrans{i}(sprev,s) + GMDP.Ploc{i}(sNprev,s,policyloc{i}(sNprev)) * Z1;
                        end
                    end
                end
            end
            % renormalize matrice
            Qtrans{i} = Qtrans{i}./repmat(sum(Qtrans{i},2),1,GMDP.M(i));
            
            % Qcond
            if t == 1
                Qcond{i} = Qtrans{i};
            else
                %for s0 = 1:GMDP.M(i) % for all s0_i
                %    for sprev = 1:GMDP.M(i) % for all sprev_i
                %        Qcond{i}(s0,s) = Qcond{i}(s0,s) + Qtrans{i}(sprev,s)*Qcond_prev{i}(s0,sprev);
                %    end
                %end
                Qcond{i} = Qcond_prev{i}*Qtrans{i};
            end
            % renormalize matrice          
            Qcond{i} = Qcond{i}./repmat(sum(Qcond{i},2),1,GMDP.M(i));
 
            % Qmarg
            %for s0 = 1:GMDP.M(i) % for all s0_i
            %    Qmarg{i}(s) = Qmarg{i}(s) + Qcond{i}(s0,s) * Qloc0{i}(s0);
            %end
            Qmarg{i}=Qcond{i}'*Qloc0{i};
            % renormalize matrice
            Qmarg{i} = Qmarg{i}/sum(Qmarg{i});
        end

        % evaluate Vloc
        for i=1:n  % for each site
            for sN0 = 1:SN(i) % for all s0_{N_i}
                v_sN0 = GMDP.VN{i}(sN0,:);
                Z1 = 0;
                for sN = 1:SN(i) % for all s_{N_i}
                    v_sN = GMDP.VN{i}(sN,:);
                    Z2 = 1;
                    for ln = 1:length(GMDP.N{i})
                        kn = GMDP.N{i}(ln);
                        Z2 = Z2 * Qcond{kn}(v_sN0(ln),v_sN(ln));
                    end
                    Z1 = Z1 + GMDP.Rloc{i}(sN,policyloc{i}(sN)) * Z2;
                end
                Vloc{i}(sN0) = Vloc{i}(sN0) + (discount^t)*Z1;
            end
        end
        
        % Prepare next loop
        Qcond_prev = Qcond;
        Qmarg_prev = Qmarg;
        t = t+1;
    end
end
