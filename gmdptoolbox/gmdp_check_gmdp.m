function [isOK, problems, GMDP] = gmdp_check_gmdp(GMDP)

% gmdp_check_gmdp    Check if a Graph-based MDP (GMDP) structure is valid
% Arguments -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% Evaluation -------------------------------------------------------------
% isOK : true if GMDP is valid, false otherwise
% problems : cell array of problems messages

threshold = 0.000001; % for numerical comparison

% n : number of site
n = size(GMDP.M,2);

k = 0; problems=[];

%% Check key attributs
% check M : vector (1xn) of the number of state for each site
if any(size(GMDP.M)~=[1 n]) || any(GMDP.M<2) || any(GMDP.M~=floor(GMDP.M)) || any(~isfinite(GMDP.M))
  k=k+1;problems{k} = 'GMDP Toolbox ERROR: M (vector 1xn of number of state for each site, with n the number of site) must contain integer upper than 1';
end

% check B : vector (1xn) of the number of action for each site
if any(size(GMDP.B)~=[1 n]) || any(GMDP.B<2) || any(GMDP.B~=floor(GMDP.B)) || any(~isfinite(GMDP.B))
  k=k+1;problems{k} = 'GMDP Toolbox ERROR: B (vector 1xn of number of action for each site, with n the number of site) must contain integer upper than 1';
end

% check G : adjacency matrix describing the neighborhood of each site
if any(size(GMDP.G)~=[n n]) || any(any(GMDP.G~=0 & GMDP.G~=1))  || any(any(~isfinite(GMDP.G)))
  k=k+1;problems{k} = 'GMDP Toolbox ERROR: G (adjacency matrix nxn of sites neighborhood, with n the number of site) must contain only 0 and 1';
end
[isconnected, components] = is_graph_connected(GMDP.G);
if ~isconnected  
    str_components = '';
    for i=1:length(components)
        str_components = [str_components '[' int2str(components{i}) ']  '];
    end
    k=k+1;problems{k} = ['GMDP Toolbox ERROR: G defines several connected components: ' str_components];
end

if k == 0 % proceed check only if M, B and G are OK
    for i=1:n
        N{i} = find(GMDP.G(:,i))'; % vector of neighbors
        SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
    end
    % check local transition probabilities
    if any(size(GMDP.Ploc)~=[1 n])
        k=k+1;problems{k} = 'GMDP Toolbox ERROR: Ploc is a cell array 1xn, with n the number of site';
    end
    for i=1:n
        if any(any(any(~isfinite(GMDP.Ploc{i}))))
             k=k+1;problems{k} = ['GMDP Toolbox ERROR: Ploc{' int2str(i) '} has at least a Nan'];
        end           
        if size(GMDP.Ploc{i}) ~= [SN(i) GMDP.M(i) GMDP.B(i)]
            k=k+1;problems{k} = ['GMDP Toolbox ERROR: Ploc{' int2str(i) '} has not the expected size'];
        end
        isPlocOK = true;
        for a = 1:GMDP.B(i)
            if any(abs(sum(GMDP.Ploc{i}(:,:,a),2)-1)>threshold); isPlocOK=false; end
        end
        if ~isPlocOK
            k=k+1;problems{k} = ['GMDP Toolbox ERROR: at least one Ploc{' int2str(i) '}(:,:,a) is not stochastic'];
        end
    end
    
    % check local reward
    if any(size(GMDP.Rloc)~=[1 n])
        k=k+1;problems{k} = 'GMDP Toolbox ERROR: Rloc is a cell array 1xn, with n the number of site';
    end
    for i=1:n
         if any(any(any(~isfinite(GMDP.Rloc{i}))))
             k=k+1;problems{k} = ['GMDP Toolbox ERROR: Rloc{' int2str(i) '} has at least a Nan'];
        end                 
        if any(size(GMDP.Rloc{i}) ~= [SN(i) GMDP.B(i)])
            k=k+1; problems{k} = ['GMDP Toolbox ERROR: Rloc{' int2str(i) '} has not the expected size'];
        end;
    end
end

if k==0; isOK=true; else isOK=false; end

%% if OK then compute additional attributs
if isOK
    % N with N{i} a vector of neighbors
    % VN with VN{i} an array with number of neighborhood states rows and number of neighbors columns
    GMDP.N = N;
    GMDP.VN = cell(1,n);
    for i=1:n
        GMDP.VN{i} = zeros( SN(i),length(N{i}) );
        for j=1:SN(i); GMDP.VN{i}(j,:)=itov(j, GMDP.M(N{i}));end
    end
    if max(SN)<2^16  %65536
        for i=1:n; GMDP.VN{i} = uint16(GMDP.VN{i}); end
    elseif max(SN)<2^35 %4294967296
        for i=1:n; GMDP.VN{i} = uint32(GMDP.VN{i}); end
    end
end

end

  

