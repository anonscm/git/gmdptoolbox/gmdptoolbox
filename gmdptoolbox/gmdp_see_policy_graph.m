function actions = gmdp_see_policy_graph(GMDP, policyloc, states, iscoloraction)

% gmdp_see_policy_graph      Determine and visualize sites actions preconized by a policy 
%                            for a given state of sites
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% policyloc : a given policy
% states : vector of state for each site
% iscoloraction (optional) : if true: node color related to action and state written in node, 
%                            if false: node color related to state, action written in node
%                            (default value true)
% Evaluation --------------------------------------------------------------
% actions : vector of action for each site


actions=[];
% Check of arguments
n = length(GMDP.M); % number of sites
isOKstates=true; for i=1:n; if ~ismember(states(i),1:GMDP.M(i)); isOKstates =false; end; end
if ~gmdp_check_policy(GMDP, policyloc, false)
    disp('--------------------------------------------------------')
    disp('GMDP toolbox ERROR: policyloc argument is not correct !')
    disp('--------------------------------------------------------')
elseif ~isOKstates
    disp('--------------------------------------------------------')
    disp('GMDP toolbox ERROR: states must be a vector of site state !')
    disp('--------------------------------------------------------')
elseif (nargin > 3) && (iscoloraction~=true && iscoloraction~=false )
    disp('---------------------------------------------------------------')
    disp('GMDP Toolbox ERROR: isstates must be a boolean')
    disp('---------------------------------------------------------------')
else
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
    disp('--------------------------------------------------------')

    % Set optional argument
    if ~(nargin > 3); iscoloraction = true; end
    
    % set actions preconised by policy
    actions = zeros(1,n); % vector of actions
    for k=1:n
        actions(k) = policyloc{k}(vtoi(states(GMDP.N{k}),GMDP.M(GMDP.N{k})));
    end
    
    % plot graph
    if iscoloraction
        vcolor = actions; vcolorname = 'action'; 
        vwritten = states; vwrittenname = 'state'; 
    else
        vcolor = states; vcolorname = 'state'; 
        vwritten = actions; vwrittenname = 'action';
    end
    % Define a colormap (jet - default Matlab colormap)
    c=[ 0         0    0.5625
        0         0    0.6250
        0         0    0.6875
        0         0    0.7500
        0         0    0.8125
        0         0    0.8750
        0         0    0.9375
        0         0    1.0000
        0    0.0625    1.0000
        0    0.1250    1.0000
        0    0.1875    1.0000
        0    0.2500    1.0000
        0    0.3125    1.0000
        0    0.3750    1.0000
        0    0.4375    1.0000
        0    0.5000    1.0000
        0    0.5625    1.0000
        0    0.6250    1.0000
        0    0.6875    1.0000
        0    0.7500    1.0000
        0    0.8125    1.0000
        0    0.8750    1.0000
        0    0.9375    1.0000
        0    1.0000    1.0000
        0.0625    1.0000    0.9375
        0.1250    1.0000    0.8750
        0.1875    1.0000    0.8125
        0.2500    1.0000    0.7500
        0.3125    1.0000    0.6875
        0.3750    1.0000    0.6250
        0.4375    1.0000    0.5625
        0.5000    1.0000    0.5000
        0.5625    1.0000    0.4375
        0.6250    1.0000    0.3750
        0.6875    1.0000    0.3125
        0.7500    1.0000    0.2500
        0.8125    1.0000    0.1875
        0.8750    1.0000    0.1250
        0.9375    1.0000    0.0625
        1.0000    1.0000         0
        1.0000    0.9375         0
        1.0000    0.8750         0
        1.0000    0.8125         0
        1.0000    0.7500         0
        1.0000    0.6875         0
        1.0000    0.6250         0
        1.0000    0.5625         0
        1.0000    0.5000         0
        1.0000    0.4375         0
        1.0000    0.3750         0
        1.0000    0.3125         0
        1.0000    0.2500         0
        1.0000    0.1875         0
        1.0000    0.1250         0
        1.0000    0.0625         0
        1.0000         0         0
        0.9375         0         0
        0.8750         0         0
        0.8125         0         0
        0.7500         0         0
        0.6875         0         0
        0.6250         0         0
        0.5625         0         0
        0.5000         0         0
        ];
    nbcolors = size(c,1); % number of colors in the map
    nodeName = cell(n,1);
    nodeDescription = cell(n,1); % vector of descriptions (site, state, action)
    nodeColor=cell(n,1);
    for k=1:n
        nodeName{k} = int2str(vwritten(k));
        nodeDescription{k}={['Site ' int2str(k) char(10) 'State ' ...
            int2str(states(k)) ' (' int2str(GMDP.M(k)) ' possible)' ...
            char(10) 'Action ' int2str(actions(k)) ' (' ...
            int2str(GMDP.B(k)) ' feasible)']};
        nodeColor{k}=c(floor(max(1,(vcolor(k)-1)*(nbcolors)/(GMDP.B(k)-1))),:);
    end
    [~,h]=evalc('graphViz4Matlab(GMDP.G,''-nodeLabels'',nodeName,''-nodeColors'',nodeColor,''-nodeDescriptions'',nodeDescription)');
    set(h.fig,'Name',['Visualisation of a policy with ' vcolorname ' indicated by nodes color and ' vwrittenname ' written inside nodes']);
    % Sites not neighbors with themselves are circled with dot line
    for k = find(diag(GMDP.G)==0)
        set(h.nodeArray(k),'lineStyle',':','lineWidth',0.01);
    end
end



