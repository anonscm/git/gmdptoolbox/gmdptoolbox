\documentclass[a4paper]{article}

\usepackage[english]{babel} 
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{color}
\usepackage{enumitem}
\usepackage{tcolorbox}
\usepackage[top=3cm, bottom=2cm, left=4cm , right=1.5cm]{geometry}

\usepackage{tikz}

\title{\textbf{\textcolor{red}{\helvetica{G}}\textcolor{NavyBlue}{\helvetica{MDP}}}\helvetica{toolbox}: Quick start \\Version 1.1 - September, 2016}
\date{}
\author{MJ. Cros, N. Peyrard, R. Sabbadin \\ MIAT, INRA Toulouse, France}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\arginf}{arg\,inf}
\DeclareTextFontCommand{\helvetica}{\fontfamily{phv}\selectfont}
\DeclareTextFontCommand{\courrier}{\fontfamily{pcr}\selectfont}

\setlength{\parindent}{0pt}

\begin{document}

\maketitle


This is a quick start to learn how to use GMDPtoolbox.\\
\\
For a detailed presentation of formalization and resolution of a GMDP problem with the toolbox, please refer to the user documentation of the toolbox (included in the toolbox).\\

\section{Get the GMDPtoolbox}
 From the Project Forge Files page:\\
 \courrier{https://mulcyber.toulouse.inra.fr/frs/?group\_id=213}\\
download the file \courrier{gmdptoolbox.zip}.\\
Unzip the file and go to the \courrier{gmdptoolbox} directory.
On Linux system execute the following system commands: 
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{
\begin{verbatim}
unzip gmdptoolbox.zip
cd gmdptoolbox
\end{verbatim}
} \end{tcolorbox}

\vspace{0.5cm}
GMDPtoolbox relies on graphViz4Matlab toolbox to display graphs 
in functions \courrier{gmdp\_see\_neighborhood, gmdp\_see\_policy\_graph, 
gmdp\_eval\_policy\_value\_site\_contribution}. Download this toolbox, from MATLAB Central. From the web page: \\
\courrier{http://www.mathworks.com/matlabcentral/fileexchange/21652-graphviz4matlab}\\
click on the 'Download Zip' button to download the zip file in the \courrier{gmdptoolbox} directory.\\
Unzip the file.
On Linux system execute the following system command: 
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{
\begin{verbatim}
unzip graphViz4Matlab_03Mar2010.zip
\end{verbatim}
} \end{tcolorbox}
The toolbox relies on GraphViz available free from \courrier{http://www.graphviz.org}. 
On Linux, just install the graphviz package on your computer.\\
\\
Note that one of the function \courrier{gmdp\_linear\_programming} requires the availibility 
of the MATLAB Optimization toolbox.\\
Furthermore, if MATLAB Parallel Computing Toolbox is available, the functions\\ 
 \courrier{gmdp\_linear\_programming} and \courrier{gmdp\_policy\_iteration\_MF} are greatly speeded.\\
If not available, just change \courrier{parfor} in \courrier{for} in this two functions.\\
\\
To use the toolbox in a MATLAB session, add the \courrier{gmdptoolbox} and \courrier{graphViz4Matlab}
directory absolute paths to MATLAB search path. 
Execute the following MATLAB commands:
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{
\begin{verbatim}
>> GMDPtoolbox_path = pwd;
>> addpath( GMDPtoolbox_path )
>> graphViz4Matlab_path = 'graphViz4Matlab' % give the appropriate path
>> addpath( genpath( graphViz4Matlab_path ) )
\end{verbatim}
} \end{tcolorbox}

\vspace{0.5cm}
To access the PDF user documentation, open the file \courrier{doc/ReferenceManual.pdf} with an 
appropriate software.\\
For any question, first consult the FAQ on the GMDPtoolbox website:\\
\courrier{http://www7.inra.fr/mia/T/GMDPtoolbox/install.html\#FAQ}.\\
If there is no appropriate answer or to suggest a feature, add a request on the Bugs and Feature request of the project:\\
\courrier{https://mulcyber.toulouse.inra.fr/tracker/?atid=883\&group\_id=213\&func=browse}\\


\section{Create a tiny GMDP epidemics management problem on 3 crop fields}
Use the epidemio example provided with the toolbox to generate a problem of epidemics management on 3 crop fields. Each field can be in 2 states (1: uninfected, 2: infected). On each field, 2 actions are possible (1: normal cultural mode, 2: field fallow and treat). To have more detail (transition probabilities, rewards) on the problem, read the description of the function \courrier{gmdp\_example\_epidemio} in the user documentation.\\
\\
For this problem the neighborhood relation corresponds to a spatial proximity between fields that enables epidemics propagation. The graph is the following:
\begin{center}
\begin{tikzpicture}
 \node[draw,circle,fill=gray!50,minimum width=1cm] (A) at (0,0) {1};
 \node[draw,circle,fill=gray!50,minimum width=1cm] (B) at (2,0) {2};
 \node[draw,circle,fill=gray!50,minimum width=1cm] (C) at (4,0) {3};
 \draw[<->,>=latex] (A) -- (B) ;
 \draw[<->,>=latex] (B) -- (C) ;
 \draw[->,>=latex] (A)+(0.47,0.17) arc(-90:180:0.3);
 \draw[->,>=latex] (B)+(0.47,0.17) arc(-90:180:0.3);
 \draw[->,>=latex] (C)+(0.47,0.17) arc(-90:180:0.3);
\end{tikzpicture}
\end{center}
This graph corresponds to the following adjacency matrix:
$$
G = \begin{pmatrix}
     1 & 1 & 0 \\
     1 & 1 & 1 \\
     0 & 1 & 1
    \end{pmatrix}
$$

\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{
\begin{verbatim}
>> GMDP = gmdp_example_epidemio()
GMDP = 
       M: [2 2 2]
       B: [2 2 2]
       G: [3x3 double]
    Ploc: {[4x2x2 double]  [8x2x2 double]  [4x2x2 double]}
    Rloc: {[4x2 double]  [8x2 double]  [4x2 double]}
       N: {[1 2]  [1 2 3]  [2 3]}
      VN: {[4x2 uint16]  [8x3 uint16]  [4x2 uint16]}
\end{verbatim}
} \end{tcolorbox}

Check the validity of the description with the  \courrier{gmdp\_check} function.

\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{
\begin{verbatim}
>> gmdp_check(GMDP)
ans =
     1
problems = 
    {[]}
\end{verbatim}
} \end{tcolorbox}

No error was detected.\\
\\
The graph of neighborhood relations can be vizualized with the \courrier{gmdp\_see\_neighborhood} function.\\
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{
\begin{verbatim}
>> gmdp_see_neighborhood(GMDP)
\end{verbatim}
} \end{tcolorbox}
Here is the view with the circular layout. The layout may be changed with buttons of the horizontal buttons bar. The circular layout corresponds to the button with the yellow star.\\
\\
\includegraphics[scale=0.20]{IMAGES/QS_epidemio_graph}\\

Finally, define the discount factor.
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> discount = 0.95;
\end{verbatim} 
} \end{tcolorbox} 

\section{Solve the problem}
To solve a GMDP problem, two algorithms (see the user documentation for references) are developed in the toolbox in two functions: \courrier{gmdp\_policy\_iteration\_MF} and \courrier{gmdp\_linear\_programming}. \\
The approximate policy iteration Mean-Field function \courrier{gmdp\_policy\_iteration\_MF} usually provides better approximation but it is much more time consuming. 

\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> policyloc = gmdp_policy_iteration_MF(GMDP, discount)
Iteration: 1     Current approximate global value: 124.9569
Iteration: 2     Current approximate global value: 161.6177
policyloc = 
    [4x1 double]    [8x1 double]    [4x1 double]
>> for i=1:3; disp( policyloc{i}'); end 
     1     1     2     2
     1     1     2     2     1     1     2     2
     1     2     1     2
\end{verbatim} 
} \end{tcolorbox}


If time is an issue, and if MATLAB Optimization Toolbox is available, then use the approximate linear programming algorithm.
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> policyloc = gmdp_linear_programming(GMDP, discount);
Resolve site 1
Optimization terminated.
Done site 1
Resolve site 2
Optimization terminated.
Done site 2
Resolve site 3
Optimization terminated.
Done site 3
policyloc = 
    [4x1 double]    [8x1 double]    [4x1 double]
>> for i=1:3; disp( policyloc{i}'); end
     1     1     2     2
     1     1     2     2     1     1     2     2
     1     2     1     2
\end{verbatim} 
} \end{tcolorbox} 

\vspace{0.5cm}
The same policy is found by the two approaches.\\


\section{Explore the policy found}

\subsection*{Show the policy more explicitely}
Display actions prescribed for each site and each neighbor's state.
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> gmdp_see_policy(GMDP, policyloc)
---------------------------------------------------------------
Site 1  Neighbor(s): 1  2
Actions: 1  1  2  2
---------------------------------------------------------------
Site 2  Neighbor(s): 1  2  3
Actions: 1  1  2  2  1  1  2  2
---------------------------------------------------------------
Site 3  Neighbor(s): 2  3
Actions: 1  2  1  2
\end{verbatim} 
} \end{tcolorbox} 

\subsection{Analyze the  policy}
First, we can analyse the repartition of actions prescribed by the policy (independently of sites and sites neighbors states)
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> actions_repartition = gmdp_analyze_policy(GMDP, policyloc)
\end{verbatim} 
} \end{tcolorbox} 
Here is the figure displayed.\\
\includegraphics[scale=0.30]{IMAGES/QS_sim_analyze1} \\
The policy prescribes as many times action 1 as action 2 on each site.\\
Then we can analyse the repartition at particular sites, this time depending on site states (but not on site's neighbors states). \\
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> actions_repartition_state_site = gmdp_analyze_policy_neighbor(GMDP, policyloc);
\end{verbatim} 
} \end{tcolorbox} 
\includegraphics[scale=0.30]{IMAGES/QS_sim_analyze2} \\
\\
From these repartition we can see  that the policy can be expressed as follow: \\
\emph{If field $i$ is uninfected (site state 1) then chose a normal cultural mode (action 1).\\ 
If field $i$ is infected (site state 2) then leave the field fallow and treat (action 2).} \\


\subsection{Evaluate the policy with the Mean-Field approach}
Compute an approximation of the global value function of the policy found.
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> Vloc = gmdp_eval_policy_MF (GMDP, discount, policyloc)
Vloc = 
    [4x1 double]    [8x1 double]    [4x1 double]
>>for i=1:3; disp( Vloc{i}'); end
   56.7577   56.7577   53.4217   53.4217
   52.8888   52.8888   49.9880   49.9880   52.8888   52.8888   49.9880   49.9880
   56.7577   53.4217   56.7577   53.4217
\end{verbatim} 
} \end{tcolorbox} 

We observe that, for sites 1, 2 and 3, reward is maximum when the site is not contaminated, whatever the neighborhood state.

Note that it is possible to have a better approximation of the value function 
with a better approximation of infinity (1000 instead of the default value 100)
but to the cost of a larger execution time.
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> for i=1:n; Qloc0{i}=(ones(GMDP.M(i),1)/GMDP.M(i));end
>> Vloc = gmdp_eval_policy_MF (GMDP, discount, policyloc, Qloc0, 1000)
Vloc = 
    [4x1 double]    [8x1 double]    [4x1 double]
>> for i=1:3; disp(Vloc{i}'); end
   57.0741   57.0741   53.7381   53.7381
   53.1839   53.1839   50.2831   50.2831   53.1839   53.1839   50.2831   50.2831
   57.0741   53.7381   57.0741   53.7381
\end{verbatim} 
} \end{tcolorbox} 

\subsection{Simulate policy}
Another way of evaluating the policy is to simulate the application of this policy.
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> [sim_state, sim_reward] = gmdp_simulate_policy(GMDP, policyloc);
\end{verbatim} 
} \end{tcolorbox} 
Let us now see the evolution of the global value (cumulated and instantaneous) during the 40 periods (default duration) simulated (mean value over 100 simulated trajectories).
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> value_evolution1 = gmdp_eval_policy_value(discount, sim_reward);
>> value_evolution2 = gmdp_eval_policy_value(discount, sim_reward, false);
\end{verbatim} 
} \end{tcolorbox} 
\includegraphics[scale=0.30]{IMAGES/QS_sim_value}
\includegraphics[scale=0.30]{IMAGES/QS_sim_value2}\\

The end of the cumulative global value is not flat and instantaneous global reward if not null after 40 periods (the default value), lets simulate more periods and re-estimate expected reward.
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> [sim_state, sim_reward] = gmdp_simulate_policy(GMDP, policyloc,100,100,90);
>> value_evolution3 = gmdp_eval_policy_value(discount, sim_reward);
>> value_evolution4 = gmdp_eval_policy_value(discount, sim_reward, false);
\end{verbatim} 
} \end{tcolorbox} 
\includegraphics[scale=0.30]{IMAGES/QS_sim_value3}
\includegraphics[scale=0.30]{IMAGES/QS_sim_value4}\\
This trajectory length is more adapted to the discount factor (instantaneous reward near zero at the 90th period).\\
With a length of 40 periods, the cumulative global value is: value\_evolution(40), 132.7156.\\
With a length of 90 periods, the cumulative global value is: value\_evolution(90), 151.3487.\\
With more simulations (nb\_init = 500 and nb\_run = 500), we get a close approximation and find 151.0874.\\
\\
\\
The \courrier{gmdp\_eval\_policy\_value\_site\_contribution} enables to quantify the contribution of each site to the cumulated global value.\\
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> value_site = gmdp_eval_policy_value_site_contribution(GMDP, discount, sim_reward)
value_site =
   34.1663   31.7808   34.0529
\end{verbatim} 
} \end{tcolorbox} 
\includegraphics[scale=0.20]{IMAGES/QS_sim_repartition}\\
It is quite balanced (the reason why the sites have the same color), with a slightly lower contribution of site 2.\\
\\
\\
Finally, the \courrier{gmdp\_eval\_policy\_state\_time} function enables to  analyze the time spent in the different site states.\\
\begin{tcolorbox}[notitle,boxrule=0pt,colback=gray!20,colframe=blue!20] \small{ 
\begin{verbatim}
>> state_time = gmdp_eval_policy_state_time(GMDP, sim_state);
\end{verbatim} 
} \end{tcolorbox} 
\includegraphics[scale=0.30]{IMAGES/QS_sim_time}\\
We then see that each site is nearly 30\% of the time in state contaminated (state 2, colored in brown).\\



\end{document}