function [S,A,G, Ploc, Rloc] = import_GMDP(filetoread)
    
    // this function reads a GMDP problem in a txt file
    // created using function GMDP_matlab2scilab.m
    
    
    f = mopen(filetoread, 'r');
    mgetl(f,1); // read 'N'
    N = mfscanf(f,'%i'); // read number of nodes
    
    mgetl(f,1);
    mgetl(f,1); // read 'S'
    S = ones(1,N);
    for i=1:N // vector S of  variables state space sizes
        S(i) = mfscanf(f,'%i');
    end
    
    mgetl(f,1);
    mgetl(f,1); // read 'A'
    A = ones(1,N);
    for i=1:N // vector S of actions state space sizes
        A(i) = mfscanf(f,'%i');
    end
    
    mgetl(f,1);
    mgetl(f,1); // read 'G'
    G = zeros(N,N);
    for i =1:N
        for j=1:N
            G(i,j) = mfscanf(f,'%i');
        end
        mgetl(f,1);
    end
    
    mgetl(f,1); // read 'Ploc'
    Ploc=list();
    for i=1:N
        Nei = find(G(:,i) == 1);
        Ploc(i) = hypermat([A(i) S(i) prod(S(Nei)) ]);
        for a =1:A(i)
            for s = 1:S(i)
                for sNei = 1:prod(S(Nei))
                    Ploc(i)(a,s,sNei) = mfscanf(f,'%f');
                end
                mgetl(f,1);
            end
        end
    end
    
    mgetl(f,1); // read 'Rloc'
    Rloc=list();
    for i=1:N
        Nei = find(G(:,i) == 1);
        Rloc(i) = zeros(A(i), prod(S(Nei)) );
        for a =1:A(i)
                for sNei = 1:prod(S(Nei))
                    Rloc(i)(a,sNei) = mfscanf(f,'%f');
                end
                mgetl(f,1);
        end
    end
    
    
    
    
    
    mclose(filetoread)
    
endfunction
