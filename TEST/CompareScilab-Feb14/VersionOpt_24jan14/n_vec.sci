function [Y]=n_vec(n,sizeY)
Y=[];
// Transforms integer n<prod(sizeY)+1 into a
// vector Y such that ndims(Y)=size(sizeY).
 
 
if n==0 then
  Y = 0;
else
  s = size(sizeY,2);
  Yprod = [1,cumprod(sizeY(1:s-1))];
  Y = zeros(1,s);
  m = n-1;
   
  for i = s:-1:1
    %v2 = floor(m/Yprod(i))
    Y(1,i) = %v2(:).';
    m = pmodulo(m,Yprod(i));
  end
   
  Y = Y+1;
end
endfunction