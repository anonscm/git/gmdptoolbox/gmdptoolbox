function [differ]=diff_locV(n,V_1,V_2);
         
         
// This function computes the  difference between two local value functions
//
//
// Arguments
//----------------------------------------------------------------------------------
// n          = number of nodes (vector of size of the Si, S(1)=size of s1 ??)
// v_1, v_2   = two local value functions: V(i) vector of size (S_N(i),1)
//                      
//
// Output Arguments
//----------------------------------------------------------------------------------
// differ             = the max of differences between the two value functions
//
//
//----------------------------------------------------------------------------------
 

precision = 0;

for i = 1:n
	Non_zero = find(V_1(i) ~= 0);
	precision = max(max(abs((V_1(i)(Non_zero) - V_2(i)(Non_zero))./V_1(i)(Non_zero))) , precision);
end;

differ = precision;

endfunction;