function [V,policy,iter,differ_V,differ_pol,meanV] = MF_pol_ite_opt(G,Ploc,Rloc,S,A,discount,policy0,epsilon,max_iter,max_T)



// MF_pol_ite_opt: Resolution of discounted Graph-MDP
// with approximation of the policy iteration algorithm using
// meanfield (version with quadratic complexity and optimization
// of the code)
//
// Resolution for the case of an undirected graph and i in N(i) for all i.


// Arguments --------------------------------------------------------------
//
//   G           = graph of dependencies (directed, graph list created with metanet toolbox)
//		   G is the union of the dependencies relative
//                 to the proba of transition and the dependencies relative to the rewards
//                 the functions Ploc and Rloc are based on G and not on their own dependency graph
//   S           = vector of the size of each S_i
//   A           = vector of the size of each A_i
//   Ploc        = local transition probabilities, list of length node_number(G), 
//                 Ploc(i) = hypermat([|A_i| |S_i| |S_N(i)|]);
//   Rloc        =  local rewards, list of length node_number(G), Rloc(i) = mat([|A_i| |S_N(i)|])
//   discount    = discount rate in ]0; 1]
//                 beware to check conditions of convergence for discount = 1
//   policy0     = starting policy, list of length node_number(G), 
//                 policy0(i) = vector of length |S_N(i)|,
//                 optional (default : the one which maximizes the expected immediate reward)  
//   epsilon     = epsilon for precision in policy evaluation, upper than 0,
//                 optional (default : 0.01)
//   max_iter    = maximum number of iterations to be done, upper than 0, 
//                 optional (default : 20)
//   max_T       = maximum time to approximate the infinite sum 
//                 in the approximate policy evaluation (default : 20)
//
// Evaluation -------------------------------------------------------------
//
//   V           = approximated value function, V = list(N), with N = node_number(G)
//                 V(i) = vector of length |S_N(i)|
//   policy      = approximated optimal policy
//   iter        = number of  iterations done
//   differ_pol  = percentage of differences between two successive policies, 
//               vector of size max_iter
//   differ_V    = max of differences between two successive value functions, 
//                 vector of size max_iter
//   meanV       = mean Vi(x_N(i)), mean over i and over X_N(i)
//
//Modifications from Version 2.0 -------------------------------------------
//
// 15/08/07 - ML - End iterations when the total value decreased after an iteration
// 12/12/07 - NP - Put into comments the previous modification
// 12/12/07 - NP - Remove case Rloc = mat([|A_i| |S_i|])
// 08/01/08 - NP - Suppress test on increasig value of Val defined as
//                       for i = 1:n,
//                            Val = Val + sum(V(i)) ./ size(V(i),1);
//                       end
// 24/01/08 - NP - Add a tracking of difference between policy and policy next, 
//                 and between Value and Value_next
// 25/01/08 - NP - Add a tracking of mean Vi(x_N(i)), mean over i and over X_N(i)
//                 and change stopping criterion
//
//--------------------------------------------------------------------------


N=size(S,2); // nb of sites

nargin=argn(2);
//-------
// initialization of optional arguments
//-------

if nargin < 10 then 
   max_T = 20; 
end;

if nargin < 9 then 
   max_iter = 20; 
end;

if nargin < 8 then 
   epsilon = 0.0001; 
end;

// initialization of policy: 
// the one which maximizes the expected immediate reward
if nargin < 7 then
  policy0 = greedy_policy(Rloc)
end;


iter = 0;
policy = policy0;
done = 0;

Val_prec = -%inf;
policy_prec = policy0;



differ_V = zeros(1,max_iter);
differ_pol = zeros(1,max_iter);
meanV = zeros(1,max_iter);
pol = list(10);

V = MF_pol_eva_opt(G,Ploc,Rloc,S,discount,policy,epsilon,max_T); // value of greedy policy

while ~done
//print(%io(2),iter);
          
      iter = iter + 1;
     

      V_next =  MF_pol_eva_opt(G,Ploc,Rloc,S,discount,policy,epsilon,max_T);

   //if (iter == 1) then
	//Vgreedy = V;
   //end;
      
      differ_V(iter) = diff_locV(N,V,V_next);
      V = V_next;
      
      for k = 1:N
        	meanV(iter) = meanV(iter) +  sum(V(k))/size(V(k),1);
      end;
      meanV(iter)  = meanV(iter) /N;

      policy_next = MF_pol_upd_opt(G,Ploc,Rloc,S,A,discount,V_next,policy);

      differ_pol(iter) = diff_locpol(N,policy_next,policy);
 

      if (iter > (max_iter - 10)) then
	pol(10 - max_iter + iter) = policy_next; 
      end;
 
      if (policy_next == policy | iter == max_iter) then
	//print(%io(2),'policy_next = policy');
        done = 1; 
      else
         policy_prec = policy;
         policy = policy_next;
      end;


      if (iter == max_iter) then
	v = meanV((max_iter - 10 +1):max_iter);
	[m,u] = max(v);
	policy = pol(u);
      end;

   
end;



V = MF_pol_eva_opt(G,Ploc,Rloc,S,discount,policy,epsilon,max_T);

endfunction

