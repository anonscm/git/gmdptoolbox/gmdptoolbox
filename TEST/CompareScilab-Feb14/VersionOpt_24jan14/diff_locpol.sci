function [differ]=diff_locpol(n,policy_1,policy_2);
         
         
// This function computes the percentage difference between two local policies
//
//
// Arguments
//----------------------------------------------------------------------------------
// n          = number of nodes (vector of size of the Si, S(1)=size of s1 ??)
// policy     = list of local policy vector 
//              list of length n, policy(i) = hypermat([|S_N(i)|])          
//
// Output Arguments
//--------------------------------------------------------------
// differ             = the percentage difference between the two policies
//
//
//----------------------------------------------------------------------------------
 
   
// Calculate the percentage difference between the policies
n_diff=0;
size_Polin=0;
for i=1:n
    size_Polin=size_Polin+size(policy_1(i),1);
    for j=1:size(policy_1(i),1);
        if policy_1(i)(j)~=policy_2(i)(j)
            n_diff = n_diff + 1;
        end
    end    
end
differ=n_diff/size_Polin;


endfunction;


