function [V,precision] =  MF_pol_eva_opt(G,Ploc,Rloc,S,discount,policy,epsilon,max_T) 

// MF_pol_eva_opt:  Evaluation step for 
// resolution of discounted Graph-MDP
// with approximation of the policy iteration algorithm using
// meanfield (version with quadratic complexity and optimization
// of the code)
//
// Resolution for the case of an undirected graph and i in N(i) for all i.
//  
// Arguments --------------------------------------------------------------
//
//   G         = graph of dependencies (directed, graph list created with metanet toolbox)
//   S         = vector of the size of each S_i
//   Ploc      = local transition probabilities, list of length node_number(G),
//               Ploc(i) = hypermat([|A_i| |S_i| |S_N(i)|]);
//   Rloc      = local rewards, list of length node_number(G), 
//               Rloc(i) =  mat([|A_i| |S_N(i)|])
//   discount  = discount rate in ]0; 1]
//               beware to check conditions of convergence for discount = 1
//   policy    = current policy, list of length node_number(G), 
//               policy(i) = vector of length |S_N(i)|  
//   epsilon   = epsilon policy evaluation
//   max_T     = maximum time to approximate the infinite sum in the approximate policy evaluation
//
// Evaluation -------------------------------------------------------------
//
//   V         = approximate value function, V = list(n)
//             V(i) = vector of length |S_N(i)|
//
//Modifications from Version 2.0 ------------------------------------------
//
// 
// 15/08/07 - ML -  Pdelta is computed using only matrix 2D (faster version) 
// 20/12/07 - NP -  normalisation of Pdelta, Pcond, Ptrans, Pt to avoid rounding problems
//--------------------------------------------------------------------------





//--------------------------------------------------------------
// Variables
//--------------------------------------------------------------
//  Pdelta      = list of local transitions according to delta. 
//                Pdelta(i) = Matrix [ (|S_i'|x|S_i|) , |S_N(i)\i| ]
//                
//  P(t)(i)     = list (over t) over lists i=1..N of vectors of length |S_i|
//  Ptrans(t)(i)= list (over t) over lists of matrices Qi of size |S_i'|x|S_i| by 1
//  Pcond(t)(i) = list (over t) over lists of matrices Ci of size |S_i'|x|S_i| by 1
//  P0          = list(n); P0(i) is a vector with |x_i| components. Distribution at time t = 0, P0(i) is uniform
//                 

//print(%io(2), 'entree dans eval');


//------
// Initialisation
//------
n = size(S,2);
Pdelta = list();
Ptrans = list();
Pcond = list();
P = list();
P0 = list();
pred = list(n);
size_pred = zeros(n,1);
pos = zeros(n,1);
neg_pos = list(n);
pred_but = list(n);
precision = zeros(1,max_T);

//----------
// Computation of P0
//----------
for i = 1:n,
   P0(i) = 1/S(i) * ones(S(i),1);
end 


//----------
// Computation of Pdelta 
//----------
for i = 1:n

  pred(i) = predecessors(i,G); 
  size_pred(i) = prod(S(pred(i)));
  // Index of node i in its predecessors' set.
  // Note: i should be in its predecessor set!
  pos(i) = find(pred(i) == i) 
  neg_pos(i) = find(pred(i) <> i)
  
  // Predecessors, i excepted;
  pred_but(i) = pred(i);
  pred_but(i)(pos(i)) = [];
  
 
  
  // Computation of local transition matrix Pdelta(i)(x_i'|x_i,x_N(i)\i).
  // Pdelta(i) is a |Xi|^2 by |x_N(i)\i|, the first dimension represents x_i' and x_i

  Si = size(Ploc(i),2);
  M = zeros(Si, prod(S(pred(i)))); // |x_i'| by |x_N(i)|


  for a = 1:size(Ploc(i),1)
    V_pol_i = find(policy(i) == a);
    M(:,V_pol_i) = matrix(Ploc(i)(a,:,V_pol_i),Si,size(V_pol_i,2)); //as Pdelta by not with
                                                                    // the correct dimension
  end
  
  Norm = sum(M,'r') ;
  Norm = ones(Si,1).*.Norm ;
  M = M ./ Norm ; //to ensure normalisation
 
 // Create a matrix Numbers |X_i'| by |X_N(i)|,
 // |X_i'| repetition of the same row.
 // Numbers(1,j) is the state of  x_i in state j of x_N(i)
  Numbers = 1; 
  for j = pred(i),
     if (j == i) then
	Numbers = cumsum(ones(1,S(i))) .*. Numbers;  
     else 
        Numbers = ones(1,S(j)) .*. Numbers;
     end
  end  
  Numbers = Numbers .*. ones(S(i),1);


  // Computation
  Pdelta_temp = zeros(S(i)*S(i),prod(S(pred_but(i))));
  for num = 1:S(i), //find elements of M s.t. i in in state num
     Ind = find(bool2s(Numbers == num));
     Temp = M .* bool2s(Numbers == num);
     Temp = Temp(Ind);
     Temp = matrix(Temp,S(i),prod(S(pred_but(i))));
     Pdelta_temp((1 + (num - 1) * S(i)):(num * S(i)),:) = Temp; 
  end
  Pdelta(i) = Pdelta_temp;


end;


//-------
// Initialisation of V. (term corresponding to time zero)
//-------
Vhat=list(n);


for i=1:n
    l = size(Rloc(i),2)
    R = zeros(l,1);
    for a = 1:size(Rloc(i),1)
      I = find(policy(i) == a);
      if ~isempty(I)
	R(I) = Rloc(i)(a,I)';
      end;
    end;

    Vhat(i) = zeros(l,1);
    Vhat(i) = R;

end;



//-------
// Computation loop t.
//-------
done = 0;
t = 0;
iter = 0;

while ~done  
  t = t+1;
  iter = iter +1;

  //-------
  // Computation of Ptrans(t) from P(t-1) and Pdelta.
  //-------


  Ptrans(t) = list();//Ptrans(t)(i): size |Xi|^2 by 1 (x_i' and x_i are grouped)
  if (t == 1)
    for i=1:n // here we compute for each i P0(x_N(i)\i) as a product of P0(X_j), j in N(i)\i
     PP = 1;
     if ~isempty(pred_but(i))
        for j = pred_but(i)
	  PP = P0(j).*.PP;
	end
     end 
     Ptrans(t)(i) = Pdelta(i)*PP; 
    end
  else
    for i = 1:n
      PP = 1; //will be prod (over j in pred_but_i) of P(t-1)(x_j), for all values of x_j 
      if ~isempty(pred_but(i))
        for j = pred_but(i)
	  PP = P(t-1)(j).*.PP;
	end
      end      
      U = Pdelta(i)*PP;
      U = matrix(U,S(i),S(i));
      Norm = ones(S(i),1).*.sum(U,'r');
      U = U./Norm ; //to ensure normalisation
      Ptrans(t)(i) = matrix(U,S(i)*S(i),1);
     end
  end

  //--------
  // Computation of Pcond(t) from Ptrans(t) and Pcond(t-1).
  //--------
  Pcond(t) = list(n); //Pcond(t)(i): size |Xi|^2 by 1
  if (t == 1)
    Pcond(t) = Ptrans(t)
  else
    for i=1:n
      M = matrix(Ptrans(t)(i),S(i),S(i));
      N = matrix(Pcond(t-1)(i),S(i),S(i));
      U = M*N;
      Norm = ones(S(i),1).*.sum(U,'r');
      U = U./Norm ; //to ensure normalisation
      Pcond(t)(i) = matrix(U,S(i)*S(i),1);
    end
  end
  
  
  //--------
  // Computation of P(t) from Pcond(t) and P0.
  //--------
  P(t) = list(n);//P(t)(i): size |Xi|^2 by 1
  for i=1:n
    M = matrix(Pcond(t)(i),S(i),S(i));
    N = P0(i);  
    v = M*N;
    P(t)(i) = v/(sum(v,'r')); //to ensure normalisation
  end  
  
  
 
 
  //-------
  // Computation of Vhat_t
  //-------
  P_list = list(n);
  for i=1:n

    P_list(i) = matrix(Pcond(t)(i),S(i),S(i));
  end

  dd=discount^t;
  precision(t)=0;

  for i = 1:n
    A = [1];   
    for j = pred(i)
      A = kron(P_list(j),A); 
    end

    A = A'; 
    
    s_x_N_i = prod(S(pred(i)));
    R = zeros(s_x_N_i,1);
    
    for a = 1:size(Rloc(i),1)
      I = find(policy(i) == a);
      if ~isempty(I)
	R(I) = Rloc(i)(a,I)';
      end
    end
    
    Temp = dd*A*R; 
    Vhat(i) = Vhat(i) + Temp; 

    
    Non_zero = find(Vhat(i) ~= 0); 
    precision(t) = max(max(Temp(Non_zero)./Vhat(i)(Non_zero)) , precision(t)); 
    

  end // end loop on nodes
  
//pause;

  //-------
  // Test end of loop
  //-------
  if (precision(t)<epsilon | iter == max_T)
    V = Vhat;
    done = 1;
  end
end;

endfunction
//print(%io(2),iter);
//print(%io(2),'fin de eva');







