function policyloc = update_policy_matlab(policyloc_prev, Vloc, GMDP, discount)

n=length(GMDP.B);
for i=1:n
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
end

           % compute Ci(ai, sNi)
            for i = 1:n % for each site
                for sN = 1:SN(i)
                    v_sN = itov(sN, GMDP.M(N{i}) );
                    
                    C = GMDP.Rloc{i}(sN,:);
                    for a = 1:GMDP.B(i)
                       Z1 = 0;
                       for k = find(GMDP.G(i,:)) % sites with i as neightbor
                            N_k_without_i= N{k}; 
                            N_k_without_i(N{k}==i)=[]; % N(k) without i
                            Z2 = 0;
                            for ssN =1:SN(k)
                                v_ssN  = itov(ssN,  GMDP.M(N{k})); 
                                Z3 = 1; 
                                for j = N_k_without_i
                                    % sites neightbor of k without i
                                    %N_j_and_i = find(GMDP.G(:,j).*GMDP.G(:,i))';
                                    if GMDP.G(j,i)==1      % to compare with scilab implementation
                                        N_j_and_i = [j];
                                    else
                                        N_j_and_i = [];
                                    end
                                    Z4 = 0;
                                    x_v_sN_in_v_sssN = 0;
                                    for sssN = 1:SN(j)
                                        v_sssN = itov(sssN, GMDP.M(N{j})); 
                                        is_v_sN_in_v_sssN = true;
                                        if ~isempty(N_j_and_i)
                                            for d = N_j_and_i
                                                if v_sssN(N{j}==d) ~= v_sN(N{i}==d);
                                                    is_v_sN_in_v_sssN = false;
                                                end
                                            end
                                        end
                                        if is_v_sN_in_v_sssN
                                            x_v_sN_in_v_sssN = x_v_sN_in_v_sssN + 1;
                                            Z4 = Z4 + GMDP.Ploc{j}(sssN, v_ssN(N{k}==j),policyloc_prev{j}(sssN));
                                        end
                                        
                                    end
                                    if  x_v_sN_in_v_sssN ~= 0
                                        Z3 = Z3 * Z4 / x_v_sN_in_v_sssN;
                                    end
                                end
                                Z3 = Z3 * Vloc{k}(ssN) * GMDP.Ploc{i}(sN, v_ssN(N{k}==i),a);
                                Z2 = Z2 + Z3;
                            end
                            Z1 = Z1 + Z2;
                        end
                        C(a) = C(a) + discount*Z1;
                    end
                    C
                    [~,policyloc{i}(sN)] = max(C);
                end
            end
 
