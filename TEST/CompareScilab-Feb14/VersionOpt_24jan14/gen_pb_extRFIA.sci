function [G,Ploc,Rloc] = gen_pb_extRFIA(type_graph, param_graph)

// Generate a Graph  and the associated Ploc and Rloc for the crop field epidemic
// problem (4 states and 2 actions)
//
// Arguments -------------------------------------------------------------------
// type_graph = type of interaction graph between individuals, 1: Poisson (random),
//              2: Power Law, 3: alea directed, 4: forest graph
//
// params_graph = parameters to generate the graph
//                1: list of 2 elements
//                     N =  number of nodes
//                     p = probability to create an edge
//                2: list of 4 elements
//                     N =  number of nodes
//                     Kmin, Kmax =  resp. min and max degree of the distribution
//                                   (here the degree does not take into acount the self edge)
//                     Pk  =  degree distribution, for each degree between Kmin and Kmax
//                            (matrix 1 by (Kmax -Kmin +1))
//                            example for the power law between 3 and 20: 
//                            Pk = 3:20; Pk = Pk^(-3); Pk = Pk/sum(Pk);
// 		  3: list of 3 elements
// 		       N = number of nodes
// 		       mean_nei = mean number of predecessors per node 
//		                  (before applying the max rule)
//		       max_nei  = max number of predecessors 
//                4: one parameter, length of one side of the square of stamps	
// 
// Evaluation ------------------------------------------------------------------
// GG = directed (with loops) dependency graph 
// Ploc(i,ai,si',sNi) = local transition matrices at node i
// Rloc(i,ai,si)  = local reward matrices at node i
//
//Modifications ----------------------------------------------------------------
// 
// xx/xx/xx - NF - Rloc is now mat([|A_i| |S_N(i)|]) instead of  mat([|A_i| |S_i|])
// 11/12/07 - NP - N instead of S and A as input
// 


// Vecteur des états et vecteurs des actions
N = param_graph(1);
if type_graph == 4 then
	 N = param_graph^2+1;
end;
S = 4*ones(1,N);
A = 2*ones(1,N);
n = N;


//******************************************************
// Generation of the dependency graph                  *
// G = adjacency matrix, GG = metanet graph	       *
//******************************************************

select type_graph
	case 1 then [G] = poissongraph(N,param_graph(2))
	case 2 then [G] = configgraph(N, param_graph(2), param_graph(3),param_graph(4) )
	case 3 then [G] = aleadirgraph(N, param_graph(2), param_graph(3))
	case 4 then [G] = forestgraph(param_graph)
	else break
end;




//**************************************************************************
//              Calcul des probabilites locales :                          *
//                                                                         *
// Ploc(i,s'_i,s_N(i),A_i) taille N*D*TNmax*D                              *
//     i : noeud courant                                                   *
//     s'_i : Etat sucesseur du noeud i                                    *
//     s_N(i) : Etat courant du voisinage de i                             *
//     a_i : Action au noeud i                                             *
//**************************************************************************

// Chaque noeud a 4 etats possibles :
//     - s_i=1 : non infecte
//     - s_i=2 : infecte niveau 1
//     - s_i=3 : infecte niveau 2
//     - s_i=4 : totalement infecte
DS=4;

// Deux actions possibles a chaque noeud :
//     - a_i=1 : mode de culture normal
//     - a_i=2 : Traitement+jachere
DA=2;

// Taille du plus grand voisinage :
for i=1:N
    // taille du voisinage complet pour chaque noeud
    TN(i)=size(find(G(i,:)),2);   
    // Indice du noeud i dans son voisinage
    n(i)=nnz(G(i,1:i));               
end;
TNmax=max(TN);



////////////////////////////////////////////////////////
// Calcul des tableaux Ploc //
////////////////////////////////////////////////////////

// Dimensions du tableau Ploc :
// Ploc(i,a,si,sN(i)) : NxDAxDSxDS^TN(i)

Ploc=list();

// Proba d'infection longue distance :
epsilon=0.01;
          
//proba de passage de la maladie le long d'une arete
p=0.2; 

// proba qu'un site infecte et laissé en jachere devienne sain
pni=0.9;

// Pour chaque noeud cultive :
for i=1:N
    P = hypermat([2 4 DS^TN(i)]);
    // Etats possibles du voisinage de i
    for j=1:DS^TN(i)
        vec_j=n_vec(j,DS*ones(1,TN(i)));
        ni=sum(vec_j==2);
        pinf = epsilon + (1-epsilon)*(1 - (1-p)^ni) - 0.001;

        select vec_j(n(i))
        	case 1
	    // si i  est sain
            // Probabilite d'infection de i :
            P(1,2,j)= pinf;
            P(1,1,j)= 1 - P(1,2,j) - 0.001;
            P(1,3,j)= 0.0005;
            P(1,4,j)= 0.0005;

            P(2,1,j)= 1 - 0.001;
            P(2,2,j)= 0.001/3;
            P(2,3,j)= 0.001/3;
            P(2,4,j)= 0.001/3;

		case 2 then
            // Si i est infecte niveau 1 et cultive,
	    // il le reste ou passe au niveau d'infection superieur:
            P(1,3,j)= pinf;
            P(1,2,j)= 1 - P(1,3,j) - 0.001;
            P(1,1,j)=0.0005;
            P(1,4,j)=0.0005;
            // Mode de culture jachere : a=2
            // Si on laisse le noeud i (infecte) en jachere,
            // il devient non infecte avec proba pni :
            P(2,1,j)=pni- 0.001;
            P(2,2,j)=1-pni- 0.001;
            P(2,3,j)=0.001;
            P(2,4,j)=0.001;
            
		case 3 then
            // Si i est infecte niveau 2 et cultive,
	    // il le reste ou passe au niveau d'infection superieur:
            P(1,4,j)= pinf;
            P(1,3,j)= 1 - P(1,4,j) - 0.001;
            P(1,1,j)=0.0005;
            P(1,2,j)=0.0005;
            // Mode de culture jachere : a=2
            // Si on laisse le noeud i (infecte) en jachere,
            // il baisse de niveau avec proba pni :
            P(2,1,j)=pni/2;
            P(2,2,j)=pni/2;
            P(2,3,j)= 1 - pni -  0.001;
            P(2,4,j)= 0.001;

		case 4 then
            // Si i est totalement  infecte et cultive,
	    // il le reste:
            P(1,4,j)=1 - 0.001;
            P(1,3,j)=0.001/3;
            P(1,1,j)=0.001/3;
            P(1,2,j)=0.001/3;
            // Mode de culture jachere : a=2
            // Si on laisse le noeud i (infecte) en jachere,
            // il  baisse de niveau avec proba pni :
            P(2,1,j)=pni/3;
            P(2,2,j)=pni/3;
            P(2,3,j)=pni/3;
            P(2,4,j)= 1 - pni ;

             
        end;
    end;

Ploc(i) = P;
    
end;

//******************************************************
// Calcul des tableaux Rloc                            *
//******************************************************
//  Rloc(i) = mat([|A_i| |S_N(i)|])
//

r=100;

Rloc = list();

for i = 1:N
    I=predecessors(i,G);
    SizeS=prod(S(I));

    Ri= zeros(A(i), SizeS);
    Rloc(i)= zeros(A(i), SizeS);
	  
	  // get the position of the stand in I
	  for j=1:size(I,2)
     	 	if I(j)==i then pos=j; end;
    	  end;
    
    SizeJ=4*ones(1,size(I,2));
    
	  	  
	  for j=1:SizeS
	     j_vec=n_vec(j,SizeJ);
	     select j_vec(pos)
	         case 1 then
	             Ri(1,j)=r;
	             Ri(2,j)=0.0001;
	         case 2 then
	             Ri(1,j)=r/2;
	             Ri(2,j)=0.0001;
	         case 3 then
	             Ri(1,j)=r/3;
	             Ri(2,j)=0.0001;
	         case 4 then
	             Ri(1,j)=r/4;
	             Ri(2,j)=0.0001;
	     end;
	  end;
	  
	  Rloc(i) = Ri;
end;

endfunction





