function[policy_next] = testupd(G,Ploc,Rloc,S,A,discount,V,policy)

// MF_pol_upd_opt: updating step for
// resolution of discounted Graph-MDP
// with approximation of the policy iteration algorithm using
// meanfield (version  with quadratic complexity and optimization
// of the code)
//
// Resolution for the case of an undirected graph and i in N(i) for all i.
//  
// Arguments--------------------------------------------------------------
//
//   G             = graph of dependencies (directed, graph list created with metanet toolbox)
//   S             = vector of the size of each S_i
//   A             = vector of the size of each A_i
//   Ploc          = local transition probabilities, list of length node_number(G),
//                   Ploc(i) = hypermat([|A_i| |S_i| |S_N(i)|]);
//   Rloc          = local rewards, list of length node_number(G),
//                   Rloc(i) = mat([|A_i| |S_N(i)|])
///  discount      = discount rate in ]0; 1]
//   V             = approximate value function
//   policy        = previous policy, list of length node_number(G),
//                   policy_prev(i) = vector of length |S_N(i)| 
//
//
// Evaluation-------------------------------------------------------------
//
// policy_next = updated policy
//
//Modifications from Version 2.0 -----------------------------------------
//
// 
// 15/08/07 - ML -   Pdelta is computed using only matrix 2D (faster version) 
// 15/08/07 - ML -   No computation of Q2 : use of the exact transition from Ploc instead
// 15/08/07 - ML -   max_Tupdate not used anymore. Now immediate re-evaluation of policy,
//                   i.e. max_Tupdate = 1
// 11/12/07 - NP -   suppression of argument "epsilon".
// 14/12/07 - NP -   discount was missing in the formula. changed.
//-------------------------------------------------------------------------




//-------------------------------------------------------------
// Variables
//-------------------------------------------------------------
// Q1(i)       = List of approximate local transitions p_i^delta(y_i|x_i)
//            Q1(i) = vector [(|S_i|x|S_i|) by 1 ] (x_i, y_i)
// Q0(i)       = List of approximate local transitions p_i^delta(y_i)
//            Q0(i) = vector [(|S_i|) by 1 ]
// Pdelta          = list of local transitions according to delta. 
//             Pdelta(i) = Matrix [ (|S_i|x|S_i|) by |S_N(i)\i| ]
// K(i)(ai)    = K(i)(ai)(x_N(i)) : Value of applying ai in x_N(i)
//             and acting 'optimally' from this on.



//print(%io(2),'entre dans upd');

// Test
policy_next = policy;

//-------
// Initialisation
//-------
n = size(S,2);
pred = list(n);
size_pred = zeros(n,1);
pos = zeros(n,1);
neg_pos = list(n);
pred_but = list(n);
Q0 = list(n);
Q1 = list(n);
Pdelta = list(n);
K = list(n);
V_next = list(n);




//----------
// Computation of the approximate transitions.
//----------
for i = 1:n
  pred(i) = predecessors(i,G); 
  size_pred(i) = prod(S(pred(i)));
  pos(i) = find(pred(i) == i)
  neg_pos(i) = find(pred(i) <> i)
  pred_but(i) = pred(i);
  pred_but(i)(pos(i)) = [];
  
  // Computation of local transition matrix Pdelta(i)(x'_i_1|x_i,x_N(i)\i).
  // as in MFlinear_policy_eva_regis.
  Si = size(Ploc(i),2);
  M = zeros(Si, prod(S(pred(i))));
  for a = 1:size(Ploc(i),1)
    V_pol_i = find(policy(i) == a);
    M(:,V_pol_i) = matrix(Ploc(i)(a,:,V_pol_i),Si,size(V_pol_i,2) );
  end

 // State x_i in |x_N(i)|
  Numbers = 1; 
  for j = pred(i),
     if (j == i) then
	Numbers = cumsum(ones(1,S(i))) .*. Numbers;  
     else 
        Numbers = ones(1,S(j)) .*. Numbers;
     end
  end  
  Numbers = Numbers .*. ones(S(i),1);// Numbers is (|x'_i|,|x_N(i)|)

  // Computation
  Pdelta_temp = zeros(S(i)*S(i),prod(S(pred_but(i))));
  for num = 1:S(i),
     Ind = find(bool2s(Numbers == num))
     Temp = M .* bool2s(Numbers == num);
     Temp = Temp(Ind);
     Temp = matrix(Temp,S(i),prod(S(pred_but(i))));
     Pdelta_temp((1 + (num - 1) * S(i)):(num * S(i)),:) = Temp; 
  end
  Pdelta(i) = Pdelta_temp;
   

  // Computation of Q1 = Qtrans for current policy,  and Q0 = Qmarg for current policy
  Q1(i) = sum(Pdelta(i),2)/prod(S(pred_but(i)));
  Q0(i) = sum(matrix(Q1(i),[S(i) S(i)]),2)/S(i); //il faut donc necessairement que i soit dans N(i)
  
end

W=V;


//--------
// Computation of the improved policy.
//--------


i=1;//for i=1:n
  K(i) = list(A(i));
  ai=1;//for ai=1:A(i)
    K(i)(ai) = zeros(prod(S(pred(i))),1);
    
    for k = successors(i,G);
      M = 1;
      N = matrix(Ploc(i)(ai,:,:), [S(i) prod(S(pred(i)))]);
      M = N';
      Ind = pred(k); 
      Low = gsort(Ind(find(Ind < i)),'lc','d');
      High = Ind(find(Ind > i))
      for j = Low 
        // Case j in N(i) and in N(k)
        if or(j == intersect(pred(i),pred(k))) 
         
           N = matrix(Q1(j), [S(j) S(j)]);
           N = N';
           NN = 1
           for l = pred(i),
             if (l ~= j) then
		        NN = ones(S(l),1) .*. NN;
             else
                NN = N .*. NN;
             end
           end   
           M = (M .*. ones(1,size(NN,2))) .* ( ones(1,size(M,2)) .*. NN);  
	
	    // Case where j in N(k) and not in N(i)
	    elseif ~or(j == pred(i))
	       M = kron(M,Q0(j)');          
	    end
     end
disp(M)
     for j = High 
        // Case j in N(i) and in N(k)
        if or(j == intersect(pred(i),pred(k))) 
         
          N = matrix(Q1(j), [S(j) S(j)]);
          N = N';
          NN = 1
          for l = pred(i),
             if (l ~= j) then
		        NN = ones(S(l),1) .*. NN;
             else
                NN = N .*. NN;
             end
          end   
          M = (ones(1,size(NN,2)) .*. M) .* (NN .*. ones(1,size(M,2)));  
	
	    // Case where j in N(k) and not in N(i)
	    elseif ~or(j == pred(i))
	      M = kron(Q0(j)',M);
	    end
     end
     //print(%io(2),size(M));
disp(M)
      K(i)(ai) = K(i)(ai) + M*V(k);

    end // end loop on k
    K(i)(ai) = Rloc(i)(ai,:)' + discount*K(i)(ai);
  //end //end loop on ai
  
  // Computation of the next policy and its estimated value
  [V_next(i) policy_next(i)] = max(K(i));


//end;
//disp(K(1)(1))
endfunction
//pause;     
//print(%io(2),'fin de upd');
