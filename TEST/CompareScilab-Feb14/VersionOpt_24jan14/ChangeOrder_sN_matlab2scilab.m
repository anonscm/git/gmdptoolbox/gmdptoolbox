function VVloc = ChangeOrder_sN_matlab2scilab(Vloc,GMDP)
% Vloc (sN) with the gmdptoolbox notation for sN
% VVloc (sN)with the scilab notation for sN
n = length(GMDP.M);
VVloc=cell(1,n);
for i=1:n 
    VVloc{i} = zeros(size(Vloc{i}));
    N = find(GMDP.G(:,i))'; % vector of neighbors
    SN = prod(GMDP.M(N));  % number of states for the neighbors
    MN = GMDP.M(N);  % vector of number of states for neihgbors
    for sN=1:SN
        VVloc{i}(vec_n(itov(sN,MN),MN)) = Vloc{i}(sN);
    end
end
end

%_____________________________________________________________________
% GMDPtoolbox correspondance sN -> vector of state for each state
% 1 1
% 2 1
function v = itov (i, Y)

% Tranform the integer i in a vector of integers
% following the possibility of variation defined by vector Y
% Example: itov(11, [3 2 2]) = [3 2 1]
%          itov(10, [2 3 2]) = [2 2 2]

i = i-1;
W = [fliplr( cumprod( fliplr (Y(2:length(Y))) ) ) 1];

v = zeros(1,length(W));
for t = 1:length(W)
    x = floor(i/ W(t));
    v(t) = x+1;
    i = i - x*W(t);
end
end

%_____________________________________________________________________
% article correspondance vector of state for each state -> sN
% 1 2
% 2 1
function n=vec_n(Y,sizeY)

% Transforms vector Y (1xndims(Y)) into a number n,
% knowing each coordinate i maximum sizeY(i).
 
Yprod = [1,cumprod(sizeY(1:size(Y,2)-1))];
n = (Y-1)*Yprod'+1;
end