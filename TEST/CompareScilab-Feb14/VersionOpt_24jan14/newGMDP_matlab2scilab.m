function  [S,A,G,Ploc, Rloc] = newGMDP_matlab2scilab(GMDP)

%this function transform a GMDP in matlab format into a GMDP  dor scilab
%and write it in a text file


G = GMDP.G;
S = GMDP.M;
A = GMDP.B;

n = length(S);
for i=1:n
    
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
    MN = GMDP.M(N{i});  % vector of number of states for neihgbors
    
    Rloc{i} = zeros(A(i), SN(i));
    % change order of sN
    R = zeros(size(GMDP.Rloc{i}));
    for sN = 1:SN(i)
        R(vec_n(itov(sN,MN),MN),:) = GMDP.Rloc{i} (sN,:);
    end
    Rloc{i} = R';
    
    Ploc{i} = zeros(A(i),S(i),SN(i));
    for a=1: A(i)
        % change order of sN
        P = zeros(size(GMDP.Ploc{i}(:,:,a)));
        for sN = 1:SN(i)
            P(vec_n(itov(sN,MN),MN),:,a) = GMDP.Ploc{i}(sN,:,a);
        end
        Ploc{i}(a,:,:) = P(:,:,a)';
    end
    
end

%save('GMDP.txt', 'S', 'A',  'Ploc', 'Rloc', '-ASCII'); 
fid = fopen('GMDP.txt','w');
fprintf(fid,'N\n');
fprintf(fid,'%i\n',n);
fprintf(fid,'S\n');
for i=1:n
    fprintf(fid, '%i ' , S(i));
end
fprintf(fid, '\n');
fprintf(fid,'A\n');
for i=1:n
    fprintf(fid, '%i ' , A(i));
end
fprintf(fid, '\n');
fprintf(fid,'G \n');
for i=1:n
    for j=1:n
        fprintf(fid, '%i ', G(i,j));
    end
    fprintf(fid,'\n');
end
fprintf(fid,'Ploc \n');
for i=1:n
    for a =1:A(i)
        for s=1:S(i)
            for sni=1:SN(i)
                fprintf(fid,'%f ', Ploc{i}(a,s,sni)); 
            end
            fprintf(fid, '\n');
        end
        %fprintf(fid, '\n');
    end
end
fprintf(fid,'Rloc \n');
for i=1:n
    for a =1:A(i)
            for sni=1:SN(i)
                fprintf(fid,'%f ', Rloc{i}(a,sni)); 
            end
        fprintf(fid, '\n');
    end
end

fclose(fid);

end



%_____________________________________________________________________
% GMDPtoolbox correspondance sN -> vector of state for each state
% 1 1
% 2 1
function v = itov (i, Y)

% Tranform the integer i in a vector of integers
% following the possibility of variation defined by vector Y
% Example: itov(11, [3 2 2]) = [3 2 1]
%          itov(10, [2 3 2]) = [2 2 2]

i = i-1;
W = [fliplr( cumprod( fliplr (Y(2:length(Y))) ) ) 1];

v = zeros(1,length(W));
for t = 1:length(W)
    x = floor(i/ W(t));
    v(t) = x+1;
    i = i - x*W(t);
end
end

%_____________________________________________________________________
% article correspondance vector of state for each state -> sN
% 1 2
% 2 1
function n=vec_n(Y,sizeY)

% Transforms vector Y (1xndims(Y)) into a number n,
% knowing each coordinate i maximum sizeY(i).
 
Yprod = [1,cumprod(sizeY(1:size(Y,2)-1))];
n = (Y-1)*Yprod'+1;
end