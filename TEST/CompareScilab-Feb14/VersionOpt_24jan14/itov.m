function v = itov (i, Y)

% Tranform the integer i in a vector of integers
% following the possibility of variation defined by vector Y
% Example: itov(11, [3 2 2]) = [3 2 1]
%          itov(10, [2 3 2]) = [2 2 2]

i = i-1;
W = [fliplr( cumprod( fliplr (Y(2:length(Y))) ) ) 1];

v = zeros(1,length(W));
for t = 1:length(W)
    x = floor(i/ W(t));
    v(t) = x+1;
    i = i - x*W(t);
end
end