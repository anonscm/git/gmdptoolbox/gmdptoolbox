function n=vec_n(Y,sizeY)

% Transforms vector Y (1xndims(Y)) into a number n,
% knowing each coordinate i maximum sizeY(i).
 
Yprod = [1,cumprod(sizeY(1:size(Y,2)-1))];
n = (Y-1)*Yprod'+1;
end
