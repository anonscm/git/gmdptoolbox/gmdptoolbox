function[G] = poissongraph(N, p)


// Random graph : each pair of nodes is considered in turn,
// an edge is created with probability p
// the resulting degree distribution is a binomial law Bin(N,p): mean = (N-1)p, Var = (N-1)p(1-p)
// when N is large, it is approximated by a Poisson law : mean = var = Np
// then the edges from i to i are added, so that the mean degree is eventually Np + 1.
//
//  
// Arguments ---------------------------------------------------
//
//   N          =  number of vertices
//   p          =  proba of creation of an edge
//
// Evaluation --------------------------------------------------
//
//   G      =  oriented dependency graph in Matrix format
//--------------------------------------------------------------

G =   rand(N,N);
G(find(G > p)) = 0;
G(find(G > 0)) = 1;


G = tril(G) + tril(G)'; 

for i = 1:N
G(i,i) = 1;
end;


// check the histogram of the degrees
//v=full(sum(G,'r'));
//histplot((min(v)+0.5):(max(v)+0.5),v);

endfunction