function [policy] = greedy_policy(Rloc)

//
// 
// 
//Arguments ----------------------------------------------------
//
// Rloc =   local reward function, list,, Rloc(i) = mat([|A_i| |S_N(i)|])
//
//Evaluation --------------------------------------------------------------
//
// policy = a greedy policy wrt Rloc.
//--------------------------------------------------------------


n = size(Rloc);

policy = list(n);

for i = 1:n
  s_n_i = size(Rloc(i),2);
  policy(i) = zeros(s_n_i,1);
  for j = 1:s_n_i
    [v,k] = max(Rloc(i)(:,j));
    policy(i)(j) = k;
  end
end

endfunction
  
