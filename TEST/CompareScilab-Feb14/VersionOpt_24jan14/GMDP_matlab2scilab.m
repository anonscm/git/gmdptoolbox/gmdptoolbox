function  [S,A,G,Ploc, Rloc] = GMDP_matlab2scilab(GMDP)

%this function transform a GMDP in matlab format into a GMDP  dor scilab
%and write it in a text file


G = GMDP.G;
S = GMDP.M;
A = GMDP.B;

n = length(S);
for i=1:n
    
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
    
    Rloc{i} = zeros(A(i), SN(i));
    Rloc{i} = (GMDP.Rloc{i})';
    
    Ploc{i} = zeros(A(i),S(i),SN(i));
    for a=1: A(i)
        Ploc{i}(a,:,:) = (GMDP.Ploc{i}(:,:,a))';
    end
    
end

%save('GMDP.txt', 'S', 'A',  'Ploc', 'Rloc', '-ASCII'); 
fid = fopen('GMDP.txt','w');
fprintf(fid,'N\n');
fprintf(fid,'%i\n',n);
fprintf(fid,'S\n');
for i=1:n
    fprintf(fid, '%i ' , S(i));
end
fprintf(fid, '\n');
fprintf(fid,'A\n');
for i=1:n
    fprintf(fid, '%i ' , A(i));
end
fprintf(fid, '\n');
fprintf(fid,'G \n');
for i=1:n
    for j=1:n
        fprintf(fid, '%i ', G(i,j));
    end
    fprintf(fid,'\n');
end
fprintf(fid,'Ploc \n');
for i=1:n
    for a =1:A(i)
        for s=1:S(i)
            for sni=1:SN(i)
                fprintf(fid,'%f ', Ploc{i}(a,s,sni)); 
            end
            fprintf(fid, '\n');
        end
        %fprintf(fid, '\n');
    end
end
fprintf(fid,'Rloc \n');
for i=1:n
    for a =1:A(i)
            for sni=1:SN(i)
                fprintf(fid,'%f ', Rloc{i}(a,sni)); 
            end
        fprintf(fid, '\n');
    end
end

fclose(fid)

end