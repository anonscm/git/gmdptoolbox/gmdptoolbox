function [is_connected, components] = is_graph_connected(G)

% is_graph_connected   Check if the undirected graph corresponding to 
%                      G (adjacency matrix nxn) is connected
% Arguments ---------------------------------------------------------------
% G : adjacency matrix (nxn) 
% Evaluation --------------------------------------------------------------
% b : a structure describing the GMDP (see joint documentation)
% components : cell array of connected components

n = size(G,1);
g = G+eye(n); g = (g+g'~=0); [p,~,r]=dmperm(g);

% test the size of the first connected component
is_connected = false; if r(2)-r(1) == n; is_connected = true; end

% define components
for i=2:length(r)
    components{i-1} = p(r(i-1):r(i)-1);
end
