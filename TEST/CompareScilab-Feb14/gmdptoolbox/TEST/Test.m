% Test the GMDP toolbox functions
% for a given GMDP and discount 
% Note: Assume that n, N and SN are defined


disp (['Execute ' case_name '  **************************************']);

threshold = 0.0001; % for numerical comparison
n = length(GMDP.M); % number of site
for i=1:n
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
end


% Test 1 - Test P1, R1 - gmdp_globalize
[P, R] = gmdp_globalize(GMDP);

if any(any(any(abs(P - P1) > threshold))) || any(any(R - R1 > threshold))
    disp '   Test 1 failed';
else
    disp '   Test 1 succeed';
end


% Test 2 - Test Vloc2 - gmdp_eval_policy_MF with default arguments
for i=1:n; policyloc{i}=ones(SN(i),1);end;
for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
Tend = 100;

Vloc = gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend);

is_OK = true;
for i=1:n
    if any(isnan(Vloc{i})); is_OK = false; end;
    if any(abs(Vloc{i} - Vloc2{i}) > threshold); is_OK = false; end
end
if ~is_OK       
    disp '   Test 2 failed';
else
    disp '   Test 2 succeed';
end


% Test 3 - Test Vloc3 - gmdp_eval_policy_MF with different policyloc, Qloc0
for i=1:n; policyloc{i}=GMDP.B(i)*ones(SN(i),1);end;
for i=1:n; Qloc0{i} = rand(GMDP.M(i),1); Qloc0{i} = Qloc0{i}/sum(Qloc0{i}); end
Tend = 100;

Vloc = gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend);

is_OK = true;
for i=1:n
    if any(isnan(Vloc{i})); is_OK = false; end;
    if any(abs(Vloc{i} - Vloc3{i}) > threshold); is_OK = false; end
end
if ~is_OK       
    disp '   Test 3 failed';
else
    disp '   Test 3 succeed';
end


% Test 4 - Test Vloc4 - gmdp_eval_policy_MF with different policyloc, Qloc0, Tend
for i=1:n; for sN=1:SN(i); policyloc{i}(sN)=randi(GMDP.B(i));end;end
for i=1:n; Qloc0{i} = rand(GMDP.M(i),1); Qloc0{i} = Qloc0{i}/sum(Qloc0{i}); end
Tend = 1;

Vloc = gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend);

is_OK = true;
for i=1:n
    if any(isnan(Vloc{i})); is_OK = false; end;
    if any(abs(Vloc{i} - Vloc4{i}) > threshold); is_OK = false; end
end
if ~is_OK       
    disp '   Test 4 failed';
else
    disp '   Test 4 succeed';
end



% Test 5 - Test Vloc5, policyloc5, iter5 - Test gmdp_policy_iteration_MF with default arguments
max_iter=10;
for i=1:n; policyloc0{i}=ones(SN(i),1);end;
for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
Tend_eval=100;
max_iter_update=10;

[policyloc, Vloc, iter] = gmdp_policy_iteration_MF( GMDP,discount,max_iter,policyloc0,Qloc0,Tend_eval,max_iter_update);

is_OK = true;
for i=1:n; 
    if any(isnan(Vloc{i})); is_OK = false; end;
    if any((abs(Vloc{i} - Vloc5{i})) > threshold); is_OK = false; end; 
    if any(abs(policyloc{i} - policyloc5{i}) > threshold); is_OK = false; end; 
end
if iter ~= iter5; is_OK = false; end; 
if ~is_OK       
    disp '   Test 5 failed';
else
    disp '   Test 5 succeed';
end



%% Test 6 - Test Vloc6, policyloc6, iter6 - Test gmdp_policy_iteration_MF with different policyloc, max_iter and max_iter_update
max_iter=2;
for i=1:n; policyloc0{i}=GMDP.B(i)*ones(SN(i),1);end;
for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
Tend_eval=100;
max_iter_update=2;

[policyloc, Vloc, iter] = gmdp_policy_iteration_MF( GMDP,discount,max_iter,policyloc0,Qloc0,Tend_eval,max_iter_update);

is_OK = true;
for i=1:n; 
    if any(isnan(Vloc{i})); is_OK = false; end;
    if any((abs(Vloc{i} - Vloc6{i})) > threshold); is_OK = false; end; 
    if any(abs(policyloc{i} - policyloc6{i}) > threshold); is_OK = false; end; 
end
if iter ~= iter6; is_OK = false; end; 
if ~is_OK       
    disp '   Test 6 failed ';
else
    disp '   Test 6 succeed';
end


%% Test 7 - Test policyloc7 - Test gmdp_linear_programming
policyloc = gmdp_linear_programming (GMDP, discount);

is_OK = true;
for i=1:1; 
    if any(abs(policyloc{i} - policyloc7{i}) > threshold); is_OK = false; end; 
end
if ~is_OK       
    disp '   Test 7 failed ';
else
    disp '   Test 7 succeed';
end


