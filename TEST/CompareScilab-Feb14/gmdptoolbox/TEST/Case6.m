clear all
rng(1); % new way to control random number generation

case_name = 'Case6';

%% Case with a 4 sites GMDP generated with gmdp_example_rand
GMDP=gmdp_example_rand(4,[2 3 2 3],[3 2 3 2],2);
discount=0.9;

%% Test 1 - Test gmdp_globalize
load (['TEST/' case_name]) % P1 R1 Vloc2 Vloc3 Vloc4 Vloc5 policyloc5 Vloc6 policyloc6 policyloc7


%% Test 2 - Test gmdp_eval_policy_MF with default arguments


%% Test 3 - Test gmdp_eval_policy_MF with different policyloc and Qloc0


%% Test 4 - Test gmdp_eval_policy_MF with different policyloc, Qloc0 and Tend


%% Test 5 - Test gmdp_policy_iteration_MF with default arguments


%% Test 6 - Test gmdp_policy_iteration_MF with different policyloc, max_iter and max_iter_update


%% Test 7 - Test gmdp_linear_programming
