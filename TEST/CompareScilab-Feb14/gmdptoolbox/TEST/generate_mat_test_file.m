% Generate the expected values of a Case
% After having executed the initialization of GMDP and discount

n=length(GMDP.M);
for i=1:n
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
end
      

% Test 1 - Test P1, R1 - gmdp_globalize
[P1, R1] = gmdp_globalize(GMDP);


% Test 2 - Test Vloc2 - gmdp_eval_policy_MF with default arguments
for i=1:n; policyloc{i}=ones(SN(i),1);end;
for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
Tend = 100;

Vloc2 = gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend);


% Test 3 - Test Vloc3 - gmdp_eval_policy_MF with different policyloc, Qloc0
for i=1:n; policyloc{i}=GMDP.B(i)*ones(SN(i),1);end;
for i=1:n; Qloc0{i} = rand(GMDP.M(i),1); Qloc0{i} = Qloc0{i}/sum(Qloc0{i}); end
Tend = 100;

Vloc3 = gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend);


% Test 4 - Test Vloc4 - gmdp_eval_policy_MF with different policyloc, Qloc0, Tend
for i=1:n; for sN=1:SN(i); policyloc{i}(sN)=randi(GMDP.B(i));end;end
for i=1:n; Qloc0{i} = rand(GMDP.M(i),1); Qloc0{i} = Qloc0{i}/sum(Qloc0{i}); end
Tend = 1;

Vloc4 = gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend);


% Test 5 - Test Vloc5, policyloc5, iter5 - Test gmdp_policy_iteration_MF with default arguments
max_iter=10;
for i=1:n; policyloc0{i}=ones(SN(i),1);end;
for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
Tend_eval=100;
max_iter_update=10;

[policyloc5, Vloc5, iter5] = gmdp_policy_iteration_MF( GMDP,discount,max_iter,policyloc0,Qloc0,Tend_eval,max_iter_update);


%% Test 6 - Test Vloc6, policyloc6, iter6 - Test gmdp_policy_iteration_MF with different policyloc, max_iter and max_iter_update
max_iter=2;
for i=1:n; policyloc0{i}=GMDP.B(i)*ones(SN(i),1);end;
for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
Tend_eval=100;
max_iter_update=2;

[policyloc6, Vloc6, iter6] = gmdp_policy_iteration_MF( GMDP,discount,max_iter,policyloc0,Qloc0,Tend_eval,max_iter_update);


%% Test 7 - Test policyloc7 - Test gmdp_linear_programming
policyloc7 = gmdp_linear_programming (GMDP, discount);

save_file = ['TEST/' case_name];
save (save_file, 'P1', 'R1', 'Vloc2', 'Vloc3', 'Vloc4', 'Vloc5', 'policyloc5', 'iter5', 'Vloc6', 'policyloc6', 'iter6', 'policyloc7');
