clear all
rng(1); % new way to control random number generation

case_name = 'Case3';

%% Case with a 1 site GMDP
GMDP.M=[2];
GMDP.B=[2];
GMDP.G=[1];
P(:,:,1)=[0.2 0.8; 0.7 0.3];
P(:,:,2)=[0.9 0.1; 0.4 0.6];
GMDP.Ploc{1}=P;
GMDP.Rloc{1}=[1 2; 3 4];
discount=0.9;


%% Test 1 - Test gmdp_globalize
P1 = GMDP.Ploc{1};
R1 = GMDP.Rloc{1};


%% Test 2 - Test gmdp_eval_policy_MF with default arguments
Vloc2{1}=[ 19.9305   21.3099]';


%% Test 3 - Test gmdp_eval_policy_MF with different policyloc and Qloc0
Vloc3{1}=[23.2722   26.9085]';


%% Test 4 - Test gmdp_eval_policy_MF with different policyloc, Qloc0 and Tend
Vloc4{1}=[3.3400    4.4400]';


%% Test 5 - Test gmdp_policy_iteration_MF with default arguments
policyloc5{1}=[1 2]';
Vloc5{1}=[ 28.3044   30.8467 ]';
iter5 = 3;

%% Test 6 - Test gmdp_policy_iteration_MF with different policyloc and max_iter_update
policyloc6{1}=[1 2]';
Vloc6{1}=[ 28.3044   30.8467 ]';
iter6 = 2;

%% Test 37 - Test gmdp_linear_programming
policyloc7{1}=[2 2]';
