clear all
rng(1); % new way to control random number generation

case_name = 'Case5';

%% Case with a 4 sites GMDP, sites no neighbor of themselves
GMDP.M=[2 3 4 2];
GMDP.B=[4 2 2 3];
GMDP.G=zeros(4); GMDP.G(1,3)=1;GMDP.G(2,3)=1;GMDP.G(4,1)=1;GMDP.G(4,2)=1;

n = 4; % number of site
for i=1:n
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
end

for i=1:n
    GMDP.Ploc{i} = zeros(SN(i),GMDP.M(i),GMDP.B(i));
    GMDP.Rloc{i} = zeros(SN(i),GMDP.B(i));
    for a=1:GMDP.B(i)
        GMDP.Ploc{i}(:,:,a)=rand(SN(i),GMDP.M(i));
        GMDP.Ploc{i}(:,:,a)=GMDP.Ploc{i}(:,:,a)./repmat(sum(GMDP.Ploc{i}(:,:,a),2),1,GMDP.M(i));
        GMDP.Rloc{i}(:,a) = a*[1:SN(i)]';
    end
end

discount=0.95;

%% Test 1 - Test gmdp_globalize
load (['TEST/' case_name]) % P1 R1 


%% Test 2 - Test gmdp_eval_policy_MF with default arguments
Vloc2{1}=[ 25.0381   26.0381 ]';
Vloc2{2}=[ 25.0381   26.0381 ]';
Vloc2{3}=[ 52.0663   53.0663   54.0663   55.0663   56.0663   57.0663 ]';
Vloc2{4}=[ 19.8875 ]';


%% Test 3 - Test gmdp_eval_policy_MF with different policyloc and Qloc0
Vloc3{1}=[ 110.8169  114.8169 ]';
Vloc3{2}=[ 55.4084   57.4084 ]';
Vloc3{3}=[ 97.8041   99.8041  101.8041  103.8041  105.8041  107.8041 ]';
Vloc3{4}=[ 59.6625 ]';


%% Test 4 - Test gmdp_eval_policy_MF with different policyloc, Qloc0 and Tend
Vloc4{1}=[ 6.3170    7.3170 ]';
Vloc4{2}=[ 3.3511    6.3511 ]';
Vloc4{3}=[ 8.2760    8.2760   12.2760   10.2760   16.2760   18.2760 ]';
Vloc4{4}=[ 3.9000 ]';


%% Test 5 - Test gmdp_policy_iteration_MF with default arguments
Vloc5{1}=[ 98.7461  103.7461 ]';
Vloc5{2}=[ 43.3376   46.3376 ]';
Vloc5{3}=[ 128.9352  130.9352  132.9352  134.9352  136.9352  138.9352 ]';
Vloc5{4}=[ 59.6625 ]';
policyloc5{1}=[ 3     4]';
policyloc5{2}=[ 1     2]';
policyloc5{3}=[ 2     2     2     2     2     2 ]';
policyloc5{4}=[ 3];
iter5 = 3;


%% Test 6 - Test gmdp_policy_iteration_MF with different policyloc, max_iter and max_iter_update
Vloc6{1}=[ 98.7461  103.7461 ]';
Vloc6{2}=[ 43.3376   46.3376 ]';
Vloc6{3}=[ 128.9352  130.9352  132.9352  134.9352  136.9352  138.9352 ]';
Vloc6{4}=[ 59.6625 ]';
policyloc6{1}=[ 3     4]';
policyloc6{2}=[ 1     2]';
policyloc6{3}=[ 2     2     2     2     2     2 ]';
policyloc6{4}=[ 3];
iter6 = 2;


%% Test 7 - Test gmdp_linear_programming
policyloc7{1}=[ 4     4 ]';
policyloc7{2}=[ 2     1 ]';
policyloc7{3}=[ 2     1     1     1     1     2 ]';
policyloc7{4}=[ 3 ];
