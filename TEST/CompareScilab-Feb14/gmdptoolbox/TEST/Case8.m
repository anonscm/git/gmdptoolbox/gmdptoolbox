clear all
rng(1); % new way to control random number generation

case_name = 'Case8';

%% Case with a 4 sites GMDP, complete graph
GMDP.M=[3 3 3 3];
GMDP.B=[3 3 3 3];
GMDP.G=ones(4); 

n = 4; % number of site
for i=1:n
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
end

for i=1:n
    GMDP.Ploc{i} = zeros(SN(i),GMDP.M(i),GMDP.B(i));
    GMDP.Rloc{i} = zeros(SN(i),GMDP.B(i));
    for a=1:GMDP.B(i)
        GMDP.Ploc{i}(:,:,a)=rand(SN(i),GMDP.M(i));
        GMDP.Ploc{i}(:,:,a)=GMDP.Ploc{i}(:,:,a)./repmat(sum(GMDP.Ploc{i}(:,:,a),2),1,GMDP.M(i));
        GMDP.Rloc{i}(:,a) = a*[1:SN(i)]';
    end
end

discount=0.99;

load (['TEST/' case_name]) % P1 R1 Vloc2 Vloc3 Vloc4 Vloc5 policyloc5 Vloc6 policyloc6 policyloc7

%% Test 1 - Test gmdp_globalize

%% Test 2 - Test gmdp_eval_policy_MF with default arguments

%% Test 3 - Test gmdp_eval_policy_MF with different policyloc and Qloc0

%% Test 4 - Test gmdp_eval_policy_MF with different policyloc, Qloc0 and Tend

%% Test 5 - Test gmdp_policy_iteration_MF with default arguments

%% Test 6 - Test gmdp_policy_iteration_MF with different policyloc, max_iter and max_iter_update

%% Test 7 - Test gmdp_linear_programming
