%% Tests of the GMDP toolbox
     
% quick tests
Case1; Test; % 3 sites 
Case2; Test; % 3 sites
Case3; Test; % 1 site
Case4; Test; % 2 sites
Case5; Test; % 4 sites
Case6; Test; % 4 sites

% long tests
%Case7; Test; % 3 sites, 35 mn
%Case8; Test; % 4 sites, 2 days
