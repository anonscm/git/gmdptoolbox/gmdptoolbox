function Vloc = gmdp_eval_policy_MF (GMDP, discount, policyloc, Qloc0, Tend)

% gmdp_eval_policy_MF     Policy evaluation using Mean-Field approximation 
%                         of the transition probabilities
% Arguments -------------------------------------------------------------
% Let n be the number of site, |Si| the number of state of site i
% |SNi| the number of state of the site i neighborhood
% GMDP : a structure describing the GMDP (see joint documentation)
% discount  : discount rate in ]0; 1[
% policyloc0 : a policy, 
%              a cell array (1xn) containing at place i a vector (|SNi|x1)
% Qloc0 : initial approximated probability (default uniform)
%              a cell array (1xn) containing at place i a vector (|Si|x1)
% Tend : approximation of infinity (default 100)
% Evaluation -------------------------------------------------------------
% Vloc : approximated value function associated to the given policy
%        a cell array (1xn) containing at place i a vector (|SNi|x1)

% check of arguments
threshold = 0.000001; % for numerical comparison
n = length(GMDP.M);
is_OK_policyloc = (length(policyloc)==n);
is_OK_Qloc0 = (nargin > 4) && (length(Qloc0)==n);
for i=1:n
    N{i} = find (GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighborhood
    if is_OK_policyloc && ( length(policyloc{i})~=SN(i) || any(policyloc{i}<1) || any(policyloc{i}>GMDP.B(i)) ); is_OK_policyloc = false; end
    if is_OK_Qloc0 && ( length(Qloc0{i})~=GMDP.M(i) || any(Qloc0{i} <0) || any(Qloc0{i} >1) || abs(sum(Qloc0{i})-1)>threshold ); is_OK_Qloc0 = false; end
end

Vloc=[];
if discount <= 0 || discount >= 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Discount rate must be in ]0,1[')
    disp('--------------------------------------------------------')
elseif ~is_OK_policyloc
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: policyloc is not correct')
    disp('--------------------------------------------------------')
elseif  (nargin > 4) && ~is_OK_Qloc0
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Qloc0 is not correct')
    disp('--------------------------------------------------------')
elseif (nargin > 5) && Tend < 1 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Tend must be upper than 0')
    disp('--------------------------------------------------------')
else

    % initialization of optional arguments 
    if nargin < 4; for i=1:n; Qloc0{i}=(ones(GMDP.M(i),1)/GMDP.M(i));end;end
    if nargin < 5; Tend = 100; end

    % Initialization
    Qmarg_prev = Qloc0;
    % no need of Qcond_prev for t=1
    Vloc = cell(1,n);
    for i=1:n
        Vloc{i} = zeros(SN(i),1);
        for sN = 1:SN(i); Vloc{i}(sN) = GMDP.Rloc{i}(sN, policyloc{i}(sN));end
    end

    t = 1;
    while t <= Tend
        % Compute approximate probabilities
        for i=1:n  % for each site
            
            % Qtrans
            Qtrans{i} = zeros(GMDP.M(i),GMDP.M(i)); % initialisation
            for s = 1:GMDP.M(i) % for all s_i
                for sprev = 1:GMDP.M(i) % for all sprev_i
                    for sNprev= 1:SN(i) % for all sprev_Ni
                        v_sNprev = itov(sNprev, GMDP.M(N{i})); 
                        if v_sNprev(N{i}==i) == sprev
                            Z1 = 1;
                            for ln = 1:length(N{i})
                                kn = N{i}(ln); % a neighbor site
                                if kn ~= i; 
                                    Z1=Z1*Qmarg_prev{kn}(v_sNprev(N{i}==kn)); 
                                end
                            end
                            Qtrans{i}(sprev,s) = Qtrans{i}(sprev,s) + GMDP.Ploc{i}(sNprev,s,policyloc{i}(sNprev)) * Z1;
                        end
                    end
                end
            end    
            if GMDP.G(i,i) == 0 % site not itself a neignbor
                Qt=zeros(SN(i),GMDP.M(i));
                for sN = 1:SN(i) % for all sN_i
                    Qt(sN,:) = GMDP.Ploc{i}(sN,:,policyloc{i}(sN));
                end
                Qt = sum(Qt,1)/SN(i); % mean on all neighbor states
                Qtrans{i} = repmat(Qt,GMDP.M(i),1); % same proba for each state of the site
            end  
            % renormalize matrice
            Qtrans{i} = Qtrans{i}./repmat(sum(Qtrans{i},2),1,GMDP.M(i));
            
            % Qcond
            if t == 1
                Qcond{i} = Qtrans{i};
            else
                %for s0 = 1:GMDP.M(i) % for all s0_i
                %    for sprev = 1:GMDP.M(i) % for all sprev_i
                %        Qcond{i}(s0,s) = Qcond{i}(s0,s) + Qtrans{i}(sprev,s)*Qcond_prev{i}(s0,sprev);
                %    end
                %end
                Qcond{i} = Qcond_prev{i}*Qtrans{i};
            end
            % renormalize matrice          
            Qcond{i} = Qcond{i}./repmat(sum(Qcond{i},2),1,GMDP.M(i));
 
            % Qmarg
            %for s0 = 1:GMDP.M(i) % for all s0_i
            %    Qmarg{i}(s) = Qmarg{i}(s) + Qcond{i}(s0,s) * Qloc0{i}(s0);
            %end
            Qmarg{i}=Qcond{i}'*Qloc0{i};
            % renormalize matrice
            Qmarg{i} = Qmarg{i}/sum(Qmarg{i});
        end

        % evaluate Vloc
        for i=1:n  % for each site
            for sN0 = 1:SN(i) % for all s0_{N_i}
                v_sN0 = itov(sN0, GMDP.M(N{i}));
                Z1 = 0;
                for sN = 1:SN(i) % for all s_{N_i}
                    v_sN = itov(sN, GMDP.M(N{i}));
                    Z2 = 1;
                    for ln = 1:length(N{i})
                        kn = N{i}(ln);
                        Z2 = Z2 * Qcond{kn}(v_sN0(ln),v_sN(ln));
                    end
                    Z1 = Z1 + GMDP.Rloc{i}(sN,policyloc{i}(sN)) * Z2;
                end
                Vloc{i}(sN0) = Vloc{i}(sN0) + (discount^t)*Z1;
            end
        end
        
        % Prepare next loop
        Qcond_prev = Qcond;
        Qmarg_prev = Qmarg;
        t = t+1;
    end
end
