function GMDP = gmdp_example_epidemio(pepsilon, pc, pd, r)

% gmdp_example_epidemio    generate a Graph-based MDP (GMDP) example based  
%                          on the management of 3 sites to maximize the reward 
%                          avoiding an epidemy
% 2 states for each site: sane(1), infected(2)
% 2 possible actions for each site: usual action(1), treatment(2)
% Arguments -------------------------------------------------------------
% pepsilon : probability of long distance contamination (default 0.2)
% pc : probability of contamination by a neighbor site (default 0.5)
% pd : probability of decontamination if treatment is performed (default 0.8)
% r : reward if state is sane and usual action is performed (default 4)
% Evaluation -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)


%% arguments checking
GMDP=[];
if nargin >= 1 && (pepsilon < 0 || pepsilon > 1)
  disp('----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: Probability pepsilon must be in [0; 1]')
  disp('----------------------------------------------------------')
elseif nargin >= 2 && (pc < 0 || pc > 1)
  disp('-----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: Probability pc must be in [0; 1]')
  disp('-----------------------------------------------------------')
elseif nargin >= 3 && (pd < 0 || pd > 1)
  disp('-----------------------------------------------------------')
  disp('GMDP Toolbox ERROR: Probability pd must be in [0; 1]')
  disp('-----------------------------------------------------------')
else

    % initialization of optional arguments
    if nargin < 1; pepsilon = 0.2; end;
    if nargin < 2; pc = 0.5; end;
    if nargin < 3; pd = 0.8; end;
    if nargin < 4; r=4; end;
    

    %% Defining the GMDP
    GMDP.M = [2 2 2]; % number of states in each site
    GMDP.B = [2 2 2]; % number of action in each site
    GMDP.G = [ 1 1 0;   % adjacency matrix
               1 1 1;   % (1)--(2)--(3)
               0 1 1];    
    % Local transition probabilities    
    n = length(GMDP.M);
    nni = 0:(n-1);
    pin = pepsilon + (1 - pepsilon)*(1 - (1 - pc).^nni);
    P = zeros(4,2,2);
    P(:,:,1) = [1 - pin(1)   pin(1);
                1 - pin(2)   pin(2); 
                0           1;
                0           1];
    P(:,:,2) = [1          0;
                1          0;
                pd         1-pd;
                pd         1-pd];
    GMDP.Ploc{1} = P;
    P = zeros(8,2,2);
    P(:,:,1) = [1 - pin(1)  pin(1);
                1 - pin(2)  pin(2); 
                0           1;
                0           1;
                1 - pin(2)   pin(2);
                1 - pin(3)   pin(3); 
                0           1;
                0           1];
    P(:,:,2) = [1          0;
                1          0;
                pd         1-pd;
                pd         1-pd;
                1          0;
                1          0;
                pd         1-pd;
                pd         1-pd];
    GMDP.Ploc{2} = P;
    P = zeros(4,2,2);
    P(:,:,1) = [1 - pin(1)   pin(1);
                0           1;
                1 - pin(2)   pin(2); 
                0           1];
    P(:,:,2) = [1          0;
                pd         1-pd;
                1          0;
                pd         1-pd];
    GMDP.Ploc{3} = P;

    % local rewards
    GMDP.Rloc{1} = [r   0;
                    r   0;
                    r/2 0;
                    r/2 0];
    GMDP.Rloc{2} = [r   0;
                     r   0;
                     r/2 0;
                     r/2 0;
                     r   0;
                     r   0;
                     r/2 0;
                     r/2 0];
    GMDP.Rloc{3} = [r   0;
                    r/2 0;
                    r   0;
                    r/2 0];
end
       
