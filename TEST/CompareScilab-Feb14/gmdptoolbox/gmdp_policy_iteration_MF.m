function [policyloc, Vloc, iter] = gmdp_policy_iteration_MF(GMDP,discount, max_iter, policyloc0, Qloc0, Tend_eval, max_iter_update)


% gmdp_policy_iteration_MF  Resolution of discounted Grah-based MDP 
%                           with approximated policy iteration algorithm
%                           The evaluation of a policy is done with
%                           gmdp_eval_policy_MF
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% discount : discount rate in ]0; 1[
% max_iter : maximum number of iteration to be done (default 100)
% policyloc0 : an initial policy (default ones)
% Qloc0 : initial approximated probability (default uniform)
% Tend_eval : approximation of infinity (default 100)
% max_iter_update : maximum number of iteration to update the current policy (default 10)
% Evaluation --------------------------------------------------------------
% policyloc : approximated optimum policy
% Vloc(S) : value function associated with the policy found
% iter : number of done iterations

% check of arguments
n = length(GMDP.M);
is_OK_policyloc0 = (nargin > 4) && (length(policyloc0)==n);
is_OK_Qloc0 =  (nargin > 5) && (length(Qloc0)==n);
for i=1:n
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
    if is_OK_policyloc0 && ( length(policyloc0{i})~=SN(i) || any(policyloc0{i}<1) || any(policyloc0{i}>GMDP.B(i)) ); is_OK_policyloc0 = false; end
    if is_OK_Qloc0 && ( length(Qloc0{i})~=GMDP.M(i) || any(Qloc0{i} <0) || any(Qloc0{i} >1) || sum(Qloc0{i}) ~=1 ); is_OK_Qloc0 = false; end
end

policyloc=[]; Vloc=[]; iter=[];
disp('--------------------------------------------------------')
disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
disp('--------------------------------------------------------')   
if discount <= 0 || discount >= 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Discount rate must be in ]0,1[')
    disp('--------------------------------------------------------')
elseif nargin > 3 && max_iter < 1 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: The maximum number of iteration must be upper than 0')
    disp('--------------------------------------------------------')
elseif (nargin > 4) && ~is_OK_policyloc0
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: policyloc0 is not correct')
    disp('--------------------------------------------------------')
elseif  (nargin > 5) && ~is_OK_Qloc0
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Qloc0 is not correct')
    disp('--------------------------------------------------------')
elseif (nargin > 6) && Tend_eval < 1 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Tend_eval must be upper than 0')
    disp('--------------------------------------------------------')
elseif nargin > 7 && max_iter_update < 1 
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: The maximum number of update iteration must be upper than 0')
    disp('--------------------------------------------------------')
else

    % initialization of optional arguments 
    if nargin < 3; max_iter = 100; end
    if nargin < 4; for i=1:n; policyloc0{i}=ones(SN(i),1);end ; end
    if nargin < 5; for i=1:n; Qloc0{i} = (ones(GMDP.M(i),1)/GMDP.M(i)); end; end
    if nargin < 6; Tend_eval = 100; end
    if nargin < 7; max_iter_update = 10; end
   
    
    policyloc = policyloc0; % preallocation
    policyloc_prev = policyloc0;
    iter = 0;
    is_iterate = true;
    while is_iterate
        Vloc = gmdp_eval_policy_MF(GMDP, discount, policyloc_prev, Qloc0, Tend_eval);
        q=0;for i=1:n; q=q+mean(Vloc{i}); end; q/n
        policyloc_prev_update = policyloc_prev;
        iter_update = 0;
        is_iterate_update = true;
        while is_iterate_update
            
            % compute Ci(ai, sNi)
            for i = 1:n % for each site
                for sN = 1:SN(i)
                    v_sN = itov(sN, GMDP.M(N{i}) );
                    
                    C = GMDP.Rloc{i}(sN,:);
                    for a = 1:GMDP.B(i)
                       Z1 = 0;
                       for k = find(GMDP.G(i,:)) % sites with i as neightbor
                            N_k_without_i= N{k}; 
                            N_k_without_i(N{k}==i)=[]; % N(k) without i
                            Z2 = 0;
                            for ssN =1:SN(k)
                                v_ssN  = itov(ssN,  GMDP.M(N{k})); 
                                Z3 = 1; 
                                for j = N_k_without_i
                                    % sites neightbor of k without i 
                                    N_j_and_i = find(GMDP.G(:,j).*GMDP.G(:,i))';
                                    Z4 = 0;
                                    x_v_sN_in_v_sssN = 0;
                                    for sssN = 1:SN(j)
                                        v_sssN = itov(sssN, GMDP.M(N{j})); 
                                        is_v_sN_in_v_sssN = true;
                                        if ~isempty(N_j_and_i)
                                            for d = N_j_and_i
                                                if v_sssN(N{j}==d) ~= v_sN(N{i}==d);
                                                    is_v_sN_in_v_sssN = false;
                                                end
                                            end
                                        end
                                        if is_v_sN_in_v_sssN
                                            x_v_sN_in_v_sssN = x_v_sN_in_v_sssN + 1;
                                            Z4 = Z4 + GMDP.Ploc{j}(sssN, v_ssN(N{k}==j),policyloc_prev{j}(sssN));
                                        end
                                        
                                    end
                                    if  x_v_sN_in_v_sssN ~= 0
                                        Z3 = Z3 * Z4 / x_v_sN_in_v_sssN;
                                    end
                                end
                                Z3 = Z3 * Vloc{k}(ssN) * GMDP.Ploc{i}(sN, v_ssN(N{k}==i),a);
                                Z2 = Z2 + Z3;
                            end
                            Z1 = Z1 + Z2;
                        end
                        C(a) = C(a) + discount*Z1;
                    end
                    
                    [~,policyloc{i}(sN)] = max(C);
                end
            end
            
            iter_update = iter_update + 1;
            is_eq=true; for d=1:n; if any(policyloc_prev_update{d}~=policyloc{d}); is_eq=false; end; end
            if is_eq || iter_update == max_iter_update;
                is_iterate_update = false;
            else
                policyloc_prev_update = policyloc;
            end
        end
        iter = iter + 1;
        is_eq=true; for d=1:n; if any(policyloc_prev{d}~=policyloc{d}); is_eq=false; end; end
        if is_eq || iter == max_iter
            is_iterate = false;
        else
            policyloc_prev = policyloc;
        end
    end
    Vloc = gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend_eval);
    q=0;for i=1:n; q=q+mean(Vloc{i}); end; q/n
  
end
