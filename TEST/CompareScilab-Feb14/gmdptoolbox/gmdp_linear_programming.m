function policyloc = gmdp_linear_programming (GMDP, discount)

% gmdp_linear_programming  Resolution of discounted Grah-based MDP 
%                           with approximated linear programming
% Arguments ---------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% discount : discount rate in ]0; 1[
% Evaluation --------------------------------------------------------------
% policyloc : approximated optimum policy


% check of arguments
policyloc=[];
disp('--------------------------------------------------------')
disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
disp('--------------------------------------------------------')
if discount <= 0 || discount >= 1
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Discount rate must be in ]0,1[')
    disp('--------------------------------------------------------')
else
    n = length(GMDP.M);
    for i=1:n
        N{i} = find(GMDP.G(:,i))'; % vector of neighbors
        SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
    end
    
    Phi = zeros(n,1); 
    for i=1:n     
        % resolve a LP problem for each site, min f'x such that Ax <= b
        % Dimensions A : (2*SN(i)*B(i) x (1+M(i)), b : (2*SN(i)*B(i) x 1
        %          |-1   M|         |  Rloc{i}(sN,a) |
        % with A = |-1  -M|     b = | -Rloc{i}(sN,a) |
        w = zeros(GMDP.M(i),1);
        f = [1; w];
        M = zeros(SN(i), GMDP.M(i));
        bb = zeros(SN(i)*GMDP.B(i),1);
        for ss = 1:GMDP.M(i)
             for sN=1:SN(i)
                h=0; v_sN = itov(sN, GMDP.M(N{i})); if v_sN(N{i}==i)==ss; h=1; end
                for a = 1:GMDP.B(i)
                    sLP = vtoi( [a sN], [GMDP.B(i) SN(i)] );
                    M(sLP, ss) = h - discount*GMDP.Ploc{i}(sN,ss,a);
                    bb(sLP) = GMDP.Rloc{i}(sN,a);
               end
            end
        end
        A = [-ones(2*SN(i)*GMDP.B(i),1) [M; -M]];
        b = [bb; -bb];
        x = linprog(f, A, b);
        Phi(i) = x(1);
        w = x(2:end);
        
        % define site policy
        policyloc{i}=zeros(SN(i),1);
        for sN=1:SN(i)
            % compute Ci(ai, sNi)
            C = GMDP.Rloc{i}(sN,:);
            for a = 1:GMDP.B(i)
                Z1 = 0;
                for ss = 1:GMDP.M(i)
                    Z1 = Z1 + GMDP.Ploc{i}(sN,ss,a)*w(ss);
                end
                C(a) = C(a) + discount*Z1;
            end
            [~,policyloc{i}(sN)] = max(C);
        end
    end
end
