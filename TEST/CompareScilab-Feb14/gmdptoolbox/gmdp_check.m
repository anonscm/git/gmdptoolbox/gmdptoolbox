function [is_OK, problems] = gmdp_check(GMDP)

% gmdp_check    check if a Graph-based MDP (GMDP) is valid

% Arguments -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% Evaluation -------------------------------------------------------------
% is_OK : true if GMDP is valid, false otherwise
% problems : cell array of problems messages


% n : number of site
n = size(GMDP.M,2);
for i=1:n
    N{i} = find(GMDP.G(:,i))'; % vector of neighbors
    SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
end

k = 0; problems=cell(1);

% check M : vector (1xn) of the number of state for each site
if any(size(GMDP.M)~=[1 n]) || any(GMDP.M<2) || any(GMDP.M~=floor(GMDP.M))
  k=k+1;problems{k} = 'GMDP Toolbox ERROR: M (vector 1xn of number of state for each site, with n the number of site) must contain integer upper than 1';
end

% check B : vector (1xn) of the number of action for each site
if any(size(GMDP.M)~=[1 n]) || any(GMDP.B<2) || any(GMDP.B~=floor(GMDP.B))
  k=k+1;problems{k} = 'GMDP Toolbox ERROR: B (vector 1xn of number of action for each site, with n the number of site) must contain integer upper than 1';
end

% check G : adjacency matrix describing the neighborhood of each site
if any(size(GMDP.G)~=[n n]) || any(any(GMDP.G~=0 & GMDP.G~=1)) 
  k=k+1;problems{k} = 'GMDP Toolbox ERROR: G (adjacency matrix nxn of sites neighborhood, with n the number of site) must contain only 0 and 1';
end
[is_connected, components] = is_graph_connected(GMDP.G);
if ~is_connected  
    str_components = '';
    for i=1:length(components)
        str_components = [str_components '[' num2str(components{i}) ']  '];
    end
    k=k+1;problems{k} = ['GMDP Toolbox ERROR: G defines several connected components: ' str_components];
end
    
% check local transition probabilities 
if any(size(GMDP.Ploc)~=[1 n])
   k=k+1;problems{k} = 'GMDP Toolbox ERROR: Ploc is a cell array 1xn, with n the number of site';
end
for i=1:n
    if size(GMDP.Ploc{i}) ~= [SN(i) GMDP.M(i) GMDP.B(i)]
        k=k+1;problems{k} = ['GMDP Toolbox ERROR: Ploc{' num2str(i) '} has not the expected size'];
    end
    is_Ploc_OK = true;
    for a = 1:GMDP.B(i)
        if abs(sum(GMDP.Ploc{i}(:,:,a),2)-1)>eps; is_Ploc_OK=false; end
    end
    if ~is_Ploc_OK
        k=k+1;problems{k} = ['GMDP Toolbox ERROR: Ploc{' num2str(i) '} is not stochastic'];
    end
end

% check local reward
if any(size(GMDP.Rloc)~=[1 n])
   k=k+1;problems{k} = 'GMDP Toolbox ERROR: Rloc is a cell array 1xn, with n the number of site';
end
for i=1:n
    if size(GMDP.Rloc{i}) ~= [SN(i) GMDP.M(i)]
        k=k+1;problems{k} = 'GMDP Toolbox ERROR: Rloc{i} has not the expected size';
    end;
end

if k==0; is_OK=true; else is_OK=false; end





       
  

