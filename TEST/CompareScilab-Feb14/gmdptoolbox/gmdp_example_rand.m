function GMDP = gmdp_example_rand(n, M, B, maxN, G)

% gmdp_example_rand    generate a random Graph-based MDP (GMDP) example

% Arguments -------------------------------------------------------------
% n : number of site
% M : vector (1xn) of the number of state for each site
%     (by default 2 states by site)
% B : vector (1xn) of the number of action for each site
%     (by default 2 actions by site)
% maxN : maximum number of neighbor for a site
% G : adjacency matrix describing the neighborhood of each site
%     (by default a random matrix)
% Evaluation -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)

%% arguments checking
GMDP = []; 
if nargin >= 1 && (n<1 || n~=floor(n))
    disp('----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: n (the number of site) must be an integer upper than 1')
    disp('----------------------------------------------------------')
elseif nargin >= 2 && (length(M)~=n || any(M<2) || any(M~=floor(M)))
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: M (vector 1xn of number of state for each site) must contain integer upper than 1')
    disp('-----------------------------------------------------------')
elseif nargin >= 3 && (length(B)~=n || any(B<2) || any(B~=floor(B)))
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: B (vector 1xn of number of action for each site) must contain integer upper than 1')
    disp('-----------------------------------------------------------')
elseif nargin >= 4 && (maxN==0 || maxN~=floor(maxN))
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: N (maximum number of neighbor for a site) must be an integer upper than 1')
    disp('-----------------------------------------------------------')
elseif nargin >= 5 && (any(size(G)~=[n n]) || any(any(G~=0 & G~=1)) || ~is_graph_connected(G) || max(sum(G))>maxN)
    disp('-----------------------------------------------------------')
    disp('GMDP Toolbox ERROR: G (adjacency matrix nxn of sites neighborhood) must contain only 0 and 1, represent a connected graph with less than N neighbor per site')
    disp('-----------------------------------------------------------')
else
    
    % initialization of optional arguments
    if nargin < 1; n=3; end;
    if nargin < 2; M=2*ones(1,n); end;
    if nargin < 3; B=2*ones(1,n); end;
    if nargin < 4; maxN=4; end;
    if nargin < 5; G=generate_random_graph(n,maxN); end
    
    if size(G,1)~=0
        %% Defining the GMDP
        GMDP.M = M; % number of states in each site
        GMDP.B = B; % number of action in each site
        GMDP.G = G; % adjacency matrix
        
        for i=1:n
            N{i} = find(GMDP.G(:,i))'; % vector of neighbors
            SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
        end
        
        for i=1:n
            % Local transition probabilities
            GMDP.Ploc{i} = zeros(SN(i),GMDP.M(i),GMDP.B(i));
            for a=1:GMDP.B(i)
                GMDP.Ploc{i}(:,:,a)=rand(SN(i),GMDP.M(i));
                GMDP.Ploc{i}(:,:,a)=GMDP.Ploc{i}(:,:,a)./repmat(sum(GMDP.Ploc{i}(:,:,a)')',1,GMDP.M(i));
            end
            
            % Local reward
            GMDP.Rloc{i} = rand(SN(i),GMDP.B(i));
        end
    end
end

    

