function [P, R] = gmdp_globalize (GMDP)

% Convert a Graph-based MDP in a MDP as defined in the MDPtoolbox
% Arguments -------------------------------------------------------------
% GMDP : a structure describing the GMDP (see joint documentation)
% Evaluation -------------------------------------------------------------
% Let S be the number of states of the MDP, A the number of actions of the MDP
% P(SxSxA) : transition probability matrix 
% R(SxA) : reward matrix

disp('--------------------------------------------------------')
disp('GMDP Toolbox WARNING: GMDP argument is not checked by the function !')
disp('--------------------------------------------------------')

n = length(GMDP.M); % number of sites
S = prod(GMDP.M); % number of states of the MDP
A = prod(GMDP.B); % number of actions of the MDP

% initialization
P = ones(S,S,A);
R = zeros(S,A);

% Evaluate each element of matrices 
for a = 1:A
    aloc = itov(a , GMDP.B);
    for s = 1:S
        sloc = itov(s, GMDP.M);
        for i = 1:n % for each site  
            ineighbors = find(GMDP.G(:,i))';
            iloc = vtoi( sloc(ineighbors), GMDP.M(ineighbors) );
            R(s,a) = R(s,a) + GMDP.Rloc{i}(iloc, aloc(i) );

            for ss = 1:S
                ssloc = itov(ss, GMDP.M);
                P(s,ss,a) = P(s,ss,a) * GMDP.Ploc{i}(iloc,ssloc(i),aloc(i));
            end
        end
    end
end

                
                
                
                
