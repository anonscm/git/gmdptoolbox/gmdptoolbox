function G = generate_random_graph(n, maxN)

% generate_random_graph     generate a random graph of n sites which is
%                           connected and with each site having maximum
%                           maxN neighbors (beware: sum of column)
% Arguments -------------------------------------------------------------
% n : number of site
% Nmax : maximum number of neighbor of a site
%        Note that the neighbors of a site k are indicated by the 1
%        on the k COLUMN of the adjacency matrix
% Evaluation -------------------------------------------------------------
% G : adjacency matrix of the generated graph
%     if no graph is found, G is set to []


if n <= 0 || floor(n)~=n
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: n must be an integer upper than 1')
    disp('--------------------------------------------------------')
elseif maxN <= 0 || floor(maxN)~=maxN
    disp('--------------------------------------------------------')
    disp('GMDP Toolbox ERROR: Nmax must be an integer upper than 1')
    disp('--------------------------------------------------------')
else
    
    G=zeros(n);
    % define a random path to ensure connectivity
    d = randperm(n, n); for i=1:length(d)-1; G(d(i),d(i+1))=1;end
    
    % set randomly maximum maxN neighbor to each site
    for i=1:n
        N =sum(G); % actual number of neighbor
        addedNi = randi(maxN+1)-1 - N(i); % number of neighbor to add
        if addedNi>0; G(randperm(n, addedNi),i)=1; end
    end  
end