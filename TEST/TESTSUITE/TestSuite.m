function TestSuite(cmd)
% TestSuite    Test GMDPtoolbox functions

if ~strcmp(cmd,'test') && ~strcmp(cmd,'save') 
    display('ERROR: argument cmd must be test or save');
else
    if strcmp(cmd,'test'); istest=true; else istest=false; end
    TEST_FILE = 'TestSuite.mat';
    if istest; load(TEST_FILE); end
    
    rng(1);
    ep = 0.0001; % for numerical comparison

    %% Generate GMDP functions
    
    % G1 : gmdp_example_epidemio
    GMDP = gmdp_example_epidemio();
    if istest; if isequalgmdp(GMDP,GMDPG11); disp('Test G11 succeed'); else disp('Test G11 failed'); end
    else GMDPG11 = GMDP; end
    clear GMDP
    
    GMDP = gmdp_example_epidemio(0.8,0.2,0.5,8);
    if istest; if isequalgmdp(GMDP,GMDPG12); disp('Test G12 succeed'); else disp('Test G12 failed'); end
    else GMDPG12 = GMDP; end
    clear GMDP
    
    % G2 : gmdp_example_grid
    GMDP = gmdp_example_grid(5);
    if istest; if isequalgmdp(GMDP,GMDPG21); disp('Test G21 succeed'); else disp('Test G21 failed'); end
    else GMDPG21 = GMDP; end
    clear GMDP
    
    % G3 : gmdp_example_rand
    GMDP=gmdp_example_rand();
    if istest; if isequalgmdp(GMDP,GMDPG31); disp('Test G31 succeed'); else disp('Test G31 failed'); end
    else GMDPG31 = GMDP; end
    clear GMDP
    
    GMDP=gmdp_example_rand(4,[2 3 2 3],[3 2 3 2],2);
    if istest; if isequalgmdp(GMDP,GMDPG32); disp('Test G32 succeed'); else disp('Test G32 failed'); end
    else GMDPG32 = GMDP; end
    clear GMDP
    
    %% Resolve GMDP functions
    % R1 : gmdp_linear_programming
    GMDP=gmdp_example_rand(4,[2 3 2 3],[3 2 3 2],3);
    [T, policyloc] = evalc('gmdp_linear_programming (GMDP, 0.9)');
    if istest; if isequalcell(policyloc,policylocR11,ep); disp('Test R11 succeed'); else disp('Test R11 failed'); disp(T); end
    else policylocR11=policyloc; end
    clear GMDP T policyloc
    
    GMDP=create_GMDP_4sites_noitselfneighbor();
    [T, policyloc] = evalc('gmdp_linear_programming (GMDP, 0.9)');
    if istest; if isequalcell(policyloc,policylocR12,ep); disp('Test R12 succeed'); else disp('Test R12 failed'); disp(T); end
    else policylocR12=policyloc; end
    clear GMDP T policyloc
    
    GMDP=create_GMDP_1site;
    [T, policyloc] = evalc('gmdp_linear_programming (GMDP, 0.9)');
    if istest; if isequalcell(policyloc,policylocR13,ep); disp('Test R13 succeed'); else disp('Test R13 failed'); disp(T); end
    else policylocR13=policyloc; end
    clear GMDP T policyloc
    
    % R2 : gmdp_policy_iteration_MF
    GMDP=gmdp_example_rand(4,[5 3 2 3],[3 2 3 4],2);
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end
    discount=0.9; max_iter=10;
    for i=1:n; policyloc0{i}=randi(GMDP.B(i),SN(i),1);end;
    for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
    Tend_eval=100; max_iter_update=10;
    [T, policyloc, iter] = evalc('gmdp_policy_iteration_MF (GMDP,discount,max_iter,policyloc0,Qloc0,Tend_eval,max_iter_update)');
    if istest; if isequalcell(policyloc,policylocR21,ep)  && iter==iterR21; disp('Test R21 succeed'); else disp('Test R21 failed'); disp(T); end
    else policylocR21=policyloc; iterR21=iter; end
    clear GMDP n N SN discount max_iter policyloc0 Qloc0 Tend_eval max_iter_update T policyloc iter
    
    GMDP=create_GMDP_4sites_noitselfneighbor(); 
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end
    discount=0.9; max_iter=10;
    for i=1:n; policyloc0{i}=randi(GMDP.B(i),SN(i),1);end;
    for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
    Tend_eval=100; max_iter_update=10;
    [T, policyloc, iter] = evalc('gmdp_policy_iteration_MF (GMDP,discount,max_iter,policyloc0,Qloc0,Tend_eval,max_iter_update)');
    if istest; if isequalcell(policyloc,policylocR22,ep) && iter==iterR22; disp('Test R22 succeed'); else disp('Test R22 failed'); disp(T); end
    else policylocR22=policyloc; iterR22=iter; end
    clear GMDP n N SN discount max_iter policyloc0 Qloc0 Tend_eval max_iter_update T policyloc iter
    
    GMDP=create_GMDP_1site; 
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end
    discount=0.9; max_iter=10;
    for i=1:n; policyloc0{i}=randi(GMDP.B(i),SN(i),1);end;
    for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
    Tend_eval=100; max_iter_update=10;
    [T, policyloc, iter] = evalc('gmdp_policy_iteration_MF (GMDP,discount,max_iter,policyloc0,Qloc0,Tend_eval,max_iter_update)');
    if istest; if isequalcell(policyloc,policylocR23,ep) && iter==iterR23; disp('Test R23 succeed'); else disp('Test R23 failed'); disp(T); end
    else policylocR23=policyloc; iterR23=iter; end
    clear GMDP n N SN discount max_iter policyloc0 Qloc0 Tend_eval max_iter_update T policyloc iter
    
    
    %% Explore policy functions
    
    % E1 : gmdp_analyze_policy
    GMDP=gmdp_example_grid(5);
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    [T, actions_repartition] = evalc('gmdp_analyze_policy(GMDP, policyloc)'); close
    if istest; if isequalcell(actions_repartition,actions_repartitionE11,ep); disp('Test E11 succeed'); else disp('Test E11 failed'); disp(T); end
    else actions_repartitionE11=actions_repartition; end
    clear GMDP n N SN policyloc T actions_repartition
    
    GMDP=create_GMDP_4sites_noitselfneighbor(); 
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    [T, actions_repartition] = evalc('gmdp_analyze_policy(GMDP, policyloc, [3 2])'); close
    if istest; if isequalcell(actions_repartition,actions_repartitionE12,ep); disp('Test E12 succeed'); else disp('Test E12 failed'); disp(T); end
    else actions_repartitionE12=actions_repartition; end
    clear GMDP n N SN policyloc T actions_repartition
     
    % E2 : gmdp_analyze_policy_neighbor
    GMDP=gmdp_example_rand(4,[2 6 2 3],[2 2 3 5],2);  
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    [T, actions_repartition_state_site] = evalc('gmdp_analyze_policy_neighbor(GMDP, policyloc)'); close
    if istest; if isequalcell(actions_repartition_state_site,actions_repartition_state_siteE21,ep); disp('Test E21 succeed'); else disp('Test E21 failed'); disp(T); end
    else actions_repartition_state_siteE21=actions_repartition_state_site; end
    clear GMDP n N SN policyloc T actions_repartition_state_site
    
    GMDP = create_GMDP_4sites_noitselfneighbor();
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    [T, actions_repartition_state_site] = evalc('gmdp_analyze_policy_neighbor(GMDP, policyloc, [3 2])'); close
    if istest; if isequalcell(actions_repartition_state_site,actions_repartition_state_siteE22,ep); disp('Test E22 succeed'); else disp('Test E22 failed'); disp(T); end
    else actions_repartition_state_siteE22=actions_repartition_state_site; end
    clear GMDP n N SN policyloc T actions_repartition_state_site
    
    % E3 : gmdp_eval_policy_MF
    GMDP=gmdp_example_rand(4,[3 2 2 2],[3 2 3 3],2);
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end; discount=0.9;
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
    Tend = 100;
    [T, Vloc] = evalc('gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend)');
    if istest; if isequalcell(Vloc,VlocE31,ep); disp('Test E31 succeed'); else disp('Test E31 failed'); disp(T); end
    else VlocE31=Vloc; end
    clear GMDP n N SN discount policyloc Qloc0 Tend T Vloc

    GMDP = create_GMDP_4sites_noitselfneighbor(); 
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end; discount=0.9;
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
    Tend = 100;
    [T, Vloc] = evalc('gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend)');
    if istest; if isequalcell(Vloc,VlocE32,ep); disp('Test E32 succeed'); else disp('Test E32 failed'); disp(T); end
    else VlocE32=Vloc; end
    clear GMDP n N SN discount policyloc Qloc0 Tend T Vloc
    
    GMDP = create_GMDP_2sites(); 
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end; discount=0.9;
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    for i=1:n; Qloc0{i} = ones(GMDP.M(i),1)/GMDP.M(i); end
    Tend = 100;
    [T, Vloc] = evalc('gmdp_eval_policy_MF(GMDP, discount, policyloc, Qloc0, Tend)');
    if istest; if isequalcell(Vloc,VlocE33,ep); disp('Test E33 succeed'); else disp('Test E33 failed'); disp(T); end
    else VlocE33=Vloc; end
    clear GMDP discount policyloc Qloc0 Tend T Vloc
    
    % E4 : gmdp_simulate_policy, gmdp_eval_policy_state_time, gmdp_eval_policy_value, gmdp_eval_policy_value_site_contribution
    GMDP=gmdp_example_rand(4,[4 3 2 3],[3 2 5 2],3);
    n=length(GMDP.M); for i=1:n; N{i} = find(GMDP.G(:,i))'; SN(i) = prod(GMDP.M(N{i})); end; discount=0.9;
    for i=1:n; policyloc{i}=randi(GMDP.B(i),SN(i),1);end;
    [T,sim_state, sim_reward] = evalc('gmdp_simulate_policy(GMDP, policyloc,10,10,40)');
    if istest; if all(all(all(sim_state==sim_stateE41))) && all(all(all(abs(sim_reward-sim_rewardE41)<ep))); disp('Test E41 succeed'); else disp('Test E41 failed'); disp(T); end
    else sim_stateE41=sim_state; sim_rewardE41=sim_reward; end
    
    [T, state_time] = evalc('gmdp_eval_policy_state_time(GMDP, sim_state)'); close
    if istest; if isequal(state_time,state_timeE42); disp('Test E42 succeed'); else disp('Test E42 failed'); disp(T); end
    else state_timeE42=state_time; end
     
    value_evolution = gmdp_eval_policy_value(discount, sim_reward, true); close
    if istest; if all(value_evolution==value_evolutionE43); disp('Test E43 succeed'); else disp('Test E43 failed'); end
    else value_evolutionE43=value_evolution; end
    
    value_evolution = gmdp_eval_policy_value(discount, sim_reward, false); close
    if istest; if all(value_evolution==value_evolutionE44); disp('Test E44 succeed'); else disp('Test E44 failed'); end
    else value_evolutionE44=value_evolution; end

    [T, value_site] = evalc('gmdp_eval_policy_value_site_contribution(GMDP, discount, sim_reward)'); close
    if istest; if all(value_site==value_siteE45); disp('Test E45 succeed'); else disp('Test E45 failed'); disp(T); end
    else value_siteE45=value_site; end
    
    clear GMDP discount policyloc sim_state sim_reward state_time value_evolution value_site
    
    % E5 : gmdp_see_policy_tab, gmdp_see_policy_graph
    GMDP=gmdp_example_rand(4,[2 3 2 5],[3 2 3 4],3); discount=0.9;
    n=length(GMDP.M);
    for i=1:n; policyloc{i}=randi(GMDP.B(i), size(GMDP.VN{i},1), 1);end;
    [T, policyloc_verbose] = evalc('gmdp_see_policy_tab(GMDP, policyloc, 1:n, false)');
    if istest; if isequalcell(policyloc_verbose,policyloc_verboseE51,ep); disp('Test E51 succeed'); else disp('Test E51 failed'); end
    else policyloc_verboseE51=policyloc_verbose; end
    
    [T, actions] = evalc('gmdp_see_policy_graph(GMDP, policyloc, [1 2 2 1])'); close
    if istest; if all(actions==actionsE52); disp('Test E52 succeed'); else disp('Test E52 failed'); end
    else actionsE52=actions; end
        
    clear GMDP discount policyloc policyloc_verbose actions
    
    % E6 : gmdp_eval_policy_global_value, gmdp_global_value_state
    GMDP=gmdp_example_rand(4,[2 3 2 5],[3 2 3 4],3);
    n=length(GMDP.M);
    for i=1:n; Vloc{i}=rand(size(GMDP.VN{i},1), 1);end;
    [T, V] = evalc('gmdp_eval_policy_global_value(GMDP, Vloc)');
    if istest; if all(abs(V-VE61)<ep); disp('Test E61 succeed'); else disp('Test E61 failed'); end
    else VE61=V; end
    [T, v] = evalc('gmdp_eval_policy_global_value_state(GMDP, Vloc,[2 3 1 4])'); close
    if istest; if abs(v-vE62)<ep; disp('Test E62 succeed'); else disp('Test E62 failed'); end
    else vE62=v; end          
    clear GMDP Vloc V v
    
    GMDP = gmdp_example_grid(3);
    n=length(GMDP.M);
    for i=1:n; Vloc{i}=rand(size(GMDP.VN{i},1), 1);end;
    [T, V] = evalc('gmdp_eval_policy_global_value(GMDP, Vloc)');
    if istest; if all(abs(V-VE63)<ep); disp('Test E63 succeed'); else disp('Test E63 failed'); end
    else VE63=V; end  
    [T, v] = evalc('gmdp_eval_policy_global_value_state(GMDP, Vloc,2*ones(1,n))'); close
    if istest; if abs(v-vE64)<ep; disp('Test E64 succeed'); else disp('Test E64 failed'); end
    else vE64=v; end
    clear GMDP Vloc V v

    
    %% Utility functions
    
    % U1 : gmdp_globalize
    GMDP = gmdp_example_epidemio();
    [T, P, R] = evalc('gmdp_globalize(GMDP)');
    if istest; if all(all(all(abs(P-PU11)<ep))) && all(all(abs(R-RU11)<ep)); disp('Test U11 succeed'); else disp('Test U11 failed'); end
    else PU11=P; RU11=R; end
    clear GMDP P R
    
    % U2 : gmdp_check_policy, gmdp_globalize_local_policy, gmdp_localize_global_policy
    GMDP = gmdp_example_grid(3);
    [T, policyloc] = evalc('gmdp_linear_programming (GMDP, 0.95)');
    [T, isOK, problems] = evalc('gmdp_check_policy(GMDP, policyloc, true)');
    if istest; if isOK & isempty(problems); disp('Test U21 succeed'); else disp('Test U21 failed'); end; end
        
    [T, policy] = evalc('gmdp_globalize_local_policy(GMDP, policyloc)');
    if istest; if all(policy==policyU22); disp('Test U22 succeed'); else disp('Test U22 failed'); end
    else policyU22=policy; end
    
    [T, islocal, policyloc] = evalc('gmdp_localize_global_policy(GMDP, policy)');
    if istest; if isequalcell(policyloc,policylocU23,ep); disp('Test U23 succeed'); else disp('Test U23 failed'); end
    else policylocU23=policyloc; end
    clear GMDP policyloc policy
    
    %% save values if cmd equals 'save'
    if ~istest
        save( TEST_FILE, ...
            'GMDPG11','GMDPG12', ...
            'GMDPG21', ...
            'GMDPG31','GMDPG32','GMDPG32', ...
            'policylocR11','policylocR12','policylocR13', ...
            'policylocR21','iterR21','policylocR22','iterR22','policylocR23','iterR23', ...
            'actions_repartitionE11','actions_repartitionE12', ...
            'actions_repartition_state_siteE21','actions_repartition_state_siteE22', ...
            'VlocE31','VlocE32','VlocE33', ...
            'sim_stateE41','sim_rewardE41','state_timeE42','value_evolutionE43','value_evolutionE44', 'value_siteE45', ...
            'policyloc_verboseE51','actionsE52', ...
            'VE61','vE62','VE63','vE64', ...
            'PU11','RU11','policyU22','policylocU23' )
    end
end
end





%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function isequal = isequalgmdp(GMDP1, GMDP2)
    isequal = true;
    if any(GMDP1.M~=GMDP2.M); isequal=false;
    elseif any(GMDP1.B~=GMDP2.B); isequal=false;
    elseif  any(any(GMDP1.G~=GMDP2.G)); isequal=false;
    else
        for i=1:length(GMDP1.M)
            if any(any(any(GMDP1.Ploc{i}~=GMDP2.Ploc{i}))); isequal=false;
            elseif any(any(GMDP1.Rloc{i}~=GMDP2.Rloc{i})); isequal=false; end
        end
    end
end

function isequal = isequalcell(v1,v2,ep)
    isequal = true;
    if length(v1)~=length(v2); isequal=false; 
    else
        for i=1:length(v1)
            len = length(squeeze(size(v1{i}))); 
            if len == 1 % vector
                if any(abs(v1{i}-v2{i})>ep); isequal=false; end
            elseif len == 2 % matrix
                if any(any(abs(v1{i}-v2{i})>ep)); isequal=false; end
            elseif len == 3
                if any(any(any(abs(v1{i}-v2{i})>ep))); isequal=false; end
            else
                disp('ERROR: only cell of vector, matrix and 3D array possible'); isequal=false;
            end
        end
    end
end

function GMDP = create_GMDP_2sites()
    n = 2; % number of site
    GMDP.M=[2 3];
    GMDP.B=[3 2];
    GMDP.G=[1 1;1 1];
    for i=1:n
        N{i} = find(GMDP.G(:,i))'; % vector of neighbors
        SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
    end
    for i=1:n
        GMDP.Ploc{i} = zeros(SN(i),GMDP.M(i),GMDP.B(i));
        GMDP.Rloc{i} = zeros(SN(i),GMDP.B(i));
        for a=1:GMDP.B(i)
            GMDP.Ploc{i}(:,:,a)=rand(SN(i),GMDP.M(i));
            GMDP.Ploc{i}(:,:,a)=GMDP.Ploc{i}(:,:,a)./repmat(sum(GMDP.Ploc{i}(:,:,a),2),1,GMDP.M(i));
            GMDP.Rloc{i}(:,a) = a*(1:SN(i))';
        end
    end
    [~, ~, GMDP] = gmdp_check_gmdp(GMDP);
end

function GMDP = create_GMDP_1site()
    GMDP.M=2;
    GMDP.B=2;
    GMDP.G=1;
    P(:,:,1)=[0.2 0.8; 0.7 0.3];
    P(:,:,2)=[0.9 0.1; 0.4 0.6];
    GMDP.Ploc{1}=P;
    GMDP.Rloc{1}=[1 2; 3 4];
    [~, ~, GMDP] = gmdp_check_gmdp(GMDP);
end

function GMDP = create_GMDP_4sites_noitselfneighbor()
    n = 4; % number of site
    GMDP.M=[2 3 4 2];
    GMDP.B=[4 2 2 3];
    GMDP.G=zeros(4); GMDP.G(1,3)=1;GMDP.G(2,3)=1;GMDP.G(4,1)=1;GMDP.G(4,2)=1;
    for i=1:n
        N{i} = find(GMDP.G(:,i))'; % vector of neighbors
        SN(i) = prod(GMDP.M(N{i}));  % number of states for the neighbors
    end
    for i=1:n
        GMDP.Ploc{i} = zeros(SN(i),GMDP.M(i),GMDP.B(i));
        GMDP.Rloc{i} = zeros(SN(i),GMDP.B(i));
        for a=1:GMDP.B(i)
            GMDP.Ploc{i}(:,:,a)=rand(SN(i),GMDP.M(i));
            GMDP.Ploc{i}(:,:,a)=GMDP.Ploc{i}(:,:,a)./repmat(sum(GMDP.Ploc{i}(:,:,a),2),1,GMDP.M(i));
            GMDP.Rloc{i}(:,a) = a*(1:SN(i))';
        end
    end
    [~, ~, GMDP] = gmdp_check_gmdp(GMDP);
end

